# DUMPSTER FIRE

A privacy and security focused p2p marketplace specializing in (but not limited to) digital goods. App allows its users to create listings, buy and sell items, and trade them with other users.

## Features

-   [ ] User management (create account, login, logout, ...)
-   [ ] Listings (create, update, delete, ...)
-   [ ] Search (search listings, search users, ...)
-   [ ] Cart
-   [ ] Checkout and payment (credit card, PayPal, Bitcoin, ...)
-   [ ] User profile
-   [ ] ? Communicating between users (send messages, read messages, ...)
-   [ ] ? Security (encryption, ...)
-   [ ] ? Site moderation (reputation system, reviews, reporting, ...)
-   [ ] ? Usage analytics (statistics, ...)
-   [ ] ? Direct trading (listing for listing), auctions
-   [ ] ? Favourite listings
-   [ ] ? Listing categories

## Project structure

### /

```
- apps (contains all the seperate apps necessary for the project, frontends, backends, etc.)
     |-- next (next.js frontend)
     |-- hasura (backend)
     |-- supertokens (authentication)
- packages (contains other packages that are not apps, but are used by the project, ui elements, shared configs, etc.)
     |-- config (contains project tooling configs)
     |-- graphql (generated typedefs, graphql documents)
     |-- theme (theme)
     |-- tsconfig (contains project typescript configs)
     |-- ui (a library of ui elements)
```

### apps/next

```
- src (source files)
     |-- components (react components used in the app)
     |-- pages (files in this directory are mapped to routes in the app, [see next routes](https://nextjs.org/docs/routing/introduction))
```

## Technologies

### Frontend

-   [Next.js](https://nextjs.org/) - A server-side rendering framework for React.
-   [React](https://reactjs.org/) - A JavaScript library for building user interfaces.
-   [Apollo Client](https://www.apollographql.com/) - A GraphQL client for React.
-   [Styled Components](https://www.styled-components.com/) - A library for styling React components.
-   [Material UI](https://material-ui.com/) - A UI framework for React.
-   [TypeScript](https://www.typescriptlang.org/) - A typed superset of JavaScript.

### API

-   GraphQL - A query language for APIs and a runtime for fulfilling those queries with a data store.

### Backend

-   Hasura - A GraphQL server for Postgres.
-   SuperTokens - A service for authentication and authorization.
-   Postgres - A relational database.

### Tooling

-   [Jest](https://jestjs.io/) - A JavaScript testing framework.
-   [React Testing Library](https://testing-library.com/) - A library for testing React components.
-   [Git](https://git-scm.com/) - A distributed version control system.
-   [Gitlab CI](https://about.gitlab.com/gitlab-ci/) - A continuous integration service for Git.
-   [Prettier](https://prettier.io/) - A tool to format code.
-   [Eslint](https://eslint.org/) - A linter for JavaScript and JSX.
-   [Turborepo](https://turbo.sh/) - A tool for managing Git monorepos.

### Diagram

![](./resources/diagram.svg 'diagram')

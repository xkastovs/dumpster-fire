export type UploadPublicFileInput = {
    name: string;
    mimeType: string;
    content: string;
};

export type FileResource = {
    id: string;
    name: string;
    mimeType: string;
    resourceUrl: string;
};

export type uploadPublicFileArgs = {
    file: UploadPublicFileInput;
};

import express from 'express';
import dotenv from 'dotenv';
import { Client } from 'minio';
import { Request, Response } from 'express';
import {
    FileResource,
    uploadPublicFileArgs as UploadPublicFileArgs,
} from './types';
import bodyParser from 'body-parser';
import { randomUUID } from 'crypto';

dotenv.config();

const port = process.env.PORT || 8084;

const app = express();

const bucket = process.env.BUCKET_NAME || 'content';

const client = new Client({
    accessKey: process.env.MINIO_ACCESS_KEY!,
    secretKey: process.env.MINIO_SECRET_KEY!,
    endPoint: process.env.MINIO_ENDPOINT!,
    port: +process.env.MINIO_PORT!,
    useSSL: false,
});

app.use(bodyParser.json({ limit: '10mb' }));

app.post('/upload', async (req: Request, res: Response) => {
    const params: UploadPublicFileArgs = req.body.input;

    try {
        const fileName = params.file.name;

        //! This will literally kill the server if the file is too big
        const buffer = Buffer.from(params.file.content, 'base64');
        const id = randomUUID();
        const extension = fileName.split('.').pop();
        const metadata = { 'Content-Type': params.file.mimeType };

        await client.putObject(bucket, `${id}.${extension}`, buffer, metadata);

        const result: FileResource = {
            id,
            name: fileName,
            mimeType: params.file.mimeType,
            resourceUrl: `/${bucket}/${id}.${extension}`,
        };

        return res.status(201).json(result);
    } catch (e) {
        if (e instanceof Error) {
            console.error(e);
            return res.status(500).send(e.message);
        }
        return res.status(500).send('Internal server error');
    }
});

app.listen(port, () => console.log(`File service listening on port ${port}`));

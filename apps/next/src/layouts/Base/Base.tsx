import ResponsiveAppBar from '@components/common/Navbar/Navbar';
import { FC, PropsWithChildren } from 'react';
import { styled } from '@mui/material';
import { Footer } from '@components/common/Footer';

const xs = 600;

const StyledMain = styled('main')`
    @media only screen and (max-width: ${xs}px) {
        margin: 1rem 5%;
    }
    @media only screen and (min-width: ${xs}px) {
        margin: 1rem 10%;
    }
`;

export interface BaseProps {}
export type BaseType = FC<PropsWithChildren<BaseProps>>;

const BodyRoot = styled('div')`
    min-height: 100vh;
    display: flex;

    flex-direction: column;
`;

export const Base: BaseType = ({ children }) => {
    return (
        <BodyRoot>
            <ResponsiveAppBar />
            <StyledMain sx={{ mx: { xs: 1, md: 15 }, mt: { xs: 2, md: 5 } }}>
                {children}
            </StyledMain>
            <Footer />
        </BodyRoot>
    );
};

import React, { FC, PropsWithChildren } from 'react';
import { ApolloProvider } from '@apollo/client';
import { useNProgress } from '@hooks/useNProgress';
import { useInitApolloClient } from '@hooks/useInitApolloClient';
import { initAuth } from '@utils/auth/supertokens';
import CssBaseline from '@mui/material/CssBaseline';
import { EnhancedAppProps } from '@typings/EnhancedAppProps';
import createCache from '@emotion/cache';
import { CacheProvider } from '@emotion/react';
import { ThirdPartyEmailPasswordAuth } from 'supertokens-auth-react/lib/build/recipe/thirdpartyemailpassword';
import dynamic from 'next/dynamic';

const AuthProvider = dynamic(Promise.resolve(ThirdPartyEmailPasswordAuth), {
    ssr: false,
});

// authorization
initAuth();
const cache = createCache({ key: 'next-mui' });

const Apollo: FC<PropsWithChildren<EnhancedAppProps['pageProps']>> = (
    pageProps
) => (
    <ApolloProvider client={useInitApolloClient(pageProps.initialApolloState)}>
        {pageProps.children}
    </ApolloProvider>
);

const App: FC<EnhancedAppProps> = ({ Component, pageProps }) => {
    useNProgress();
    return (
        // why would you use a provider for a cache? that's retarded
        <CacheProvider value={cache}>
            <AuthProvider requireAuth={false}>
                <Apollo>
                    {/*<MockedProvider mocks={mocks}>*/}
                    <Component {...pageProps} />
                    <CssBaseline />
                    {/*</MockedProvider>*/}
                </Apollo>
            </AuthProvider>
        </CacheProvider>
    );
};

export default App;

import Document, { Html, Head, NextScript, Main } from 'next/document';

export default class NextDocument extends Document {
    render() {
        return (
            <Html lang="cs">
                <Head>
                    <link rel="icon" href="/images/favicon.ico" />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}

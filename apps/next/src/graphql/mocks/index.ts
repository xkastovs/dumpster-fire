import { getListing } from './getListing';
import { getListings } from './getListings';

// Include your mocks in the array below:
export const mocks = [getListing, getListings];

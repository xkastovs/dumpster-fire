import { MockedResponse } from '@apollo/client/testing';
import {
    Enums_Currency_Enum,
    GetListingsDocument,
    GetListingsQuery,
    GetListingsQueryVariables,
    Order_By,
} from 'gql/generated/react';

// Pass your generated query type as parameter to `MockedResponse`
export const getListings: MockedResponse<GetListingsQuery> = {
    // Pass your generated query document to `request.query`, if there are any variables in it, pass them as `variables`
    request: {
        query: GetListingsDocument,
        variables: {} as GetListingsQueryVariables,
    },
    // After `delay` milliseconds, the mocked API will return what you specified in `result`, this is useful for simulating network latency (and creating loaders)
    delay: 1000,
    // The expected query result
    result: {
        data: {
            app_listings: [
                {
                    id: '1034c58a-7080-4d3e-a02f-311cc4b18f75',
                    price: 1000,
                    title: 'Listing',
                    created_at: '1.4.2022',
                    __typename: 'app_listings',
                    creator: {
                        id: 'yolo',
                        name: 'Yolo',
                        __typename: 'app_users',
                    },
                    currency: Enums_Currency_Enum.Czk,
                },
            ],
            app_listings_aggregate: {
                aggregate: {
                    count: 1,
                },
                __typename: 'app_listings_aggregate',
            },
        },
    },
};

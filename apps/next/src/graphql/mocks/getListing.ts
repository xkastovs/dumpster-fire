import { MockedResponse } from '@apollo/client/testing';
import {
    Enums_Currency_Enum,
    GetListingDocument,
    GetListingQuery,
} from 'gql/generated/react';

// Pass your generated query type as parameter to `MockedResponse`
export const getListing: MockedResponse<GetListingQuery> = {
    // Pass your generated query document to `request.query`, if there are any variables in it, pass them as `variables`
    request: { query: GetListingDocument, variables: { id: 'test' } },
    // After `delay` milliseconds, the mocked API will return what you specified in `result`, this is useful for simulating network latency (and creating loaders)
    delay: 1000,
    // The expected query result
    result: {
        data: {
            app_listings_by_pk: {
                id: '1234',
                price: 1000,
                title: 'Listing',
                __typename: 'app_listings',
                created_at: '1.1.2022',
                photos: [{ id: 'a', link: 'https://placekitten.com/200/300' }],
                is_open: true,
                creator: {
                    id: 'yolo',
                    name: 'Yolo',
                    __typename: 'app_users',
                },
                currency: Enums_Currency_Enum.Czk,
                description: 'Description',
            },
        },
    },
};

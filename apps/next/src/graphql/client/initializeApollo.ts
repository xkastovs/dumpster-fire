import { createApolloClient } from './createApolloClient';
import { NormalizedCacheObject } from '@apollo/client';
import isEqual from 'lodash.isequal';
import merge from 'deepmerge';

export const initializeApollo = (
    initialState?: NormalizedCacheObject,
    token?: string
) => {
    const _apolloClient = createApolloClient(
        `${process.env.BACKEND_URL ?? 'http://localhost:8080'}/v1/graphql`,
        token
    );

    // If your page has Next.js data fetching methods that use Apollo Client, the initial state
    // gets hydrated here
    if (initialState) {
        // Get existing cache, loaded during client side data fetching
        const existingCache = _apolloClient.extract();

        // Merge the existing cache into data passed from getStaticProps/getServerSideProps
        const data = merge(initialState, existingCache, {
            // combine arrays using object equality (like in sets)
            arrayMerge: (destinationArray, sourceArray) => [
                ...sourceArray,
                ...destinationArray.filter((d) =>
                    sourceArray.every((s) => !isEqual(d, s))
                ),
            ],
        });

        // Restore the cache with the merged data
        _apolloClient.cache.restore(data);
    }
    // For SSG and SSR always create a new Apollo Client
    if (typeof window === 'undefined') return _apolloClient;

    return _apolloClient;
};

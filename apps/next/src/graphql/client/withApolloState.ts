import { ApolloClient, NormalizedCacheObject } from '@apollo/client';

export const withApolloState = (
    client: ApolloClient<NormalizedCacheObject>,
    pageProps: any
) => {
    if (pageProps?.props) {
        pageProps.props.initialApolloState = client.cache.extract();
    }

    return pageProps;
};

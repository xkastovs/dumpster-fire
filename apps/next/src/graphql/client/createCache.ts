import { InMemoryCache } from '@apollo/client';
import introspectionResult from 'gql/generated/react';

export const createCache = () =>
    new InMemoryCache({
        possibleTypes: introspectionResult.possibleTypes,
    });

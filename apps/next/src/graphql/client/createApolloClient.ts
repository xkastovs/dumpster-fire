import {
    ApolloClient,
    HttpLink,
    ApolloLink,
    FetchPolicy,
} from '@apollo/client';
import fetch from 'cross-fetch';
import { setContext } from 'apollo-link-context';
import { createCache } from './createCache';

const FETCH_POLICY: FetchPolicy = 'cache-first';

export const createApolloClient = (uri: string, token: string | undefined) => {
    const httpLink = new HttpLink({ uri, fetch });
    const authLink = setContext((_, { headers }) => {
        if (!token) return { headers };
        return {
            headers: {
                ...headers,
                Authorization: `Bearer ${token}`,
            },
        };
    });

    return new ApolloClient({
        link: ApolloLink.from([
            authLink as unknown as ApolloLink,
            // errorLink as unknown as ApolloLink,
            httpLink,
        ]),
        cache: createCache(),
        connectToDevTools: process.env.NODE_ENV === 'development',
        defaultOptions: {
            query: { fetchPolicy: FETCH_POLICY },
            watchQuery: {
                fetchPolicy: FETCH_POLICY,
                nextFetchPolicy: FETCH_POLICY,
            },
        },
        ssrMode: typeof window === 'undefined',
        assumeImmutableResults: true,
    });
};

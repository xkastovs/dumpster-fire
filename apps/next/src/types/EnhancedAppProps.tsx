import { NormalizedCacheObject } from '@apollo/client';
import { AppProps } from 'next/app';

export type EnhancedAppProps = AppProps<{
    initialApolloState: NormalizedCacheObject;
}>;

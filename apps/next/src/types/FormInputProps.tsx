import { Control, FieldValues, Path } from 'react-hook-form';
import { HTMLInputTypeAttribute } from 'react';

export interface ListingFormInputProps<T extends FieldValues> {
    control: Control<T> | undefined;
    name: Path<T>;
    label: string;
    type?: HTMLInputTypeAttribute;
}

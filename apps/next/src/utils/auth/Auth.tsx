import { useEffect } from 'react';
import dynamic from 'next/dynamic';
import SuperTokens from 'supertokens-auth-react';
import { redirectToAuth } from 'supertokens-auth-react/recipe/thirdpartyemailpassword';

const SuperTokensComponent = dynamic(
    Promise.resolve(SuperTokens.getRoutingComponent),
    { ssr: false }
);

export const Auth = () => {
    useEffect(() => {
        if (!SuperTokens.canHandleRoute()) redirectToAuth();
    }, []);

    return <SuperTokensComponent />;
};

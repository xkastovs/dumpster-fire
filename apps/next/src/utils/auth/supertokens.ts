import ThirdPartyEmailPw from 'supertokens-auth-react/recipe/thirdpartyemailpassword';
import SessionReact from 'supertokens-auth-react/recipe/session';
import SuperTokensReact from 'supertokens-auth-react';
import { isBrowser } from '@utils/isBrowser';

export const appInfo = {
    appName: 'dumpster_fire',
    apiDomain: 'http://localhost:3535',
    websiteDomain: 'http://localhost:3000',
    apiBasePath: '/auth',
    websiteBasePath: '/auth',
};

export const initAuth = () => {
    // TODO: server side authorization?
    if (!isBrowser()) return;

    SuperTokensReact.init({
        appInfo,
        recipeList: [
            ThirdPartyEmailPw.init({
                signInAndUpFeature: {
                    providers: [
                        ThirdPartyEmailPw.Github.init(),
                        ThirdPartyEmailPw.Google.init(),
                        ThirdPartyEmailPw.Facebook.init(),
                        ThirdPartyEmailPw.Apple.init(),
                    ],
                    signUpForm: {
                        formFields: [
                            {
                                id: 'name',
                                optional: false,
                                validate: async (value) => {
                                    if (value == null) {
                                        return 'Name must not be undefined!';
                                    }
                                    if (`${value}`.length == 0) {
                                        return 'Name must not be empty!';
                                    }

                                    return undefined;
                                },
                                label: 'Name',
                                placeholder: 'Name',
                            },
                        ],
                    },
                },
            }),

            SessionReact.init(),
        ],
    });
};

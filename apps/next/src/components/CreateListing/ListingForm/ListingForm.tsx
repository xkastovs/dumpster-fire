import { Dropzone } from '@components/common/forms/Dropzone';
import { FormRadioGroup } from '@components/common/forms/FormRadioGroup';
import { listingOptions } from '@components/common/forms/FormRadioGroup/listingTypeOptions';
import { FormTextInput } from '@components/common/forms/FormTextInput';
import { Button } from '@mui/material';
import React, { FC, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { theme_dark } from 'theme';
import { PrimaryButton, Stack } from 'ui';
import { FormTextArea } from '@components/common/forms/FormTextArea';
import Head from 'next/head';
import { Form_ListingFragment } from 'gql/generated/react';
import { useDropzoneFiles } from '@hooks/useDropzoneFiles';
import { useFormUpsertListing } from '@hooks/useFormUpsertListing';
import { FormDropdownSelect } from '@components/common/forms/FormDropdownSelect';
import { Enums_Currency_Enum } from 'gql/generated/react';

export interface ListingFormProps {
    initialListing?: Form_ListingFragment;
}
export type ListingFormType = FC<ListingFormProps>;

export const ListingForm: ListingFormType = ({ initialListing }) => {
    const { handleSubmit, reset, control, getValues, setValue } =
        useForm<Form_ListingFragment>({
            defaultValues: {
                currency: Enums_Currency_Enum.Czk,
            },
        });

    useEffect(() => {
        reset({ ...initialListing, currency: Enums_Currency_Enum.Czk });
    }, [reset, initialListing, setValue]);

    const {
        files,
        errors,
        onDrop,
        onDropRejected,
        clearErrors,
        deleteFile,
        deleteFiles,
    } = useDropzoneFiles();
    const { onSubmit } = useFormUpsertListing(files, getValues);

    return (
        <form>
            <Head>
                <title>Create Listing</title>
            </Head>

            <Stack direction={'column'} gap={'1rem'}>
                <FormRadioGroup
                    key={'type'}
                    control={control}
                    label={'Listing type'}
                    name={'is_selling'}
                    options={listingOptions}
                />

                <FormTextInput
                    key={'title'}
                    control={control}
                    label="Title"
                    name="title"
                />

                <FormTextArea
                    key={'description'}
                    control={control}
                    label="Description"
                    name="description"
                />

                <Stack direction={'row'} gap={theme_dark.margins.defaultCard}>
                    <FormTextInput
                        key={'price'}
                        control={control}
                        type={'number'}
                        label="Price"
                        name="price"
                    />

                    <FormDropdownSelect
                        key={'currency'}
                        control={control}
                        label="Currency"
                        name="currency"
                        options={Object.values(Enums_Currency_Enum).map((v) => {
                            return { label: v.toUpperCase(), value: v };
                        })}
                    />
                </Stack>

                <Dropzone
                    errors={errors}
                    files={files}
                    onDropRejected={onDropRejected}
                    onDrop={onDrop}
                    clearFiles={deleteFile}
                    clearErrors={clearErrors}
                />

                <Stack direction={'row'} gap={theme_dark.margins.defaultCard}>
                    <PrimaryButton onClick={handleSubmit(onSubmit)}>
                        Submit
                    </PrimaryButton>

                    <Button
                        onClick={() => {
                            reset();
                            deleteFiles();
                        }}
                        variant={'outlined'}
                    >
                        Reset
                    </Button>
                </Stack>
            </Stack>
        </form>
    );
};

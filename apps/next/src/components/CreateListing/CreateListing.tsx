import { Base } from '@layouts/Base';
import { styled } from '@mui/material';
import React from 'react';
import { theme_dark } from 'theme';
import { Card, Container, Stack } from 'ui';
import { ListingForm, ListingFormType } from './ListingForm';
import { useRouter } from '@hooks/useRouter';
import { _404 } from '@components/404';
import {
    useGetUserProfileQuery,
    useGetFormListingQuery,
} from 'gql/generated/react';
import { useSessionContext } from 'supertokens-auth-react/recipe/session';

export const CreateListing = () => {
    return <StyledCreateListing />;
};

export const EditListing = () => {
    const { userId } = useSessionContext();

    const router = useRouter();
    const id = router.query.id as string;

    const response = useGetFormListingQuery({ variables: { id }, skip: !id });
    const listing = response?.data?.app_listings_by_pk;

    const user = useGetUserProfileQuery({ variables: listing?.creator })?.data
        ?.app_users_by_pk;

    if (!listing) {
        return <_404 errorMessage={'Listing was not found'} />;
    }

    if (!user) {
        return <_404 errorMessage={'Listing author was not found'} />;
    }

    if (user.id !== userId) {
        return (
            <_404
                errorMessage={
                    "Permission denied: only the listing's author can edit it."
                }
            />
        );
    }

    return <StyledCreateListing initialListing={listing} />;
};

const UnstyledCreateListing: ListingFormType = ({ initialListing }) => {
    return (
        <Base>
            <Container maxWidth={'md'}>
                <Card>
                    <Stack
                        direction={'column'}
                        gap="1rem"
                        margin={theme_dark.margins.defaultCard}
                    >
                        <Heading>Create a new listing</Heading>
                        <ListingForm initialListing={initialListing} />
                    </Stack>
                </Card>
            </Container>
        </Base>
    );
};

const Heading = styled('h1')`
    text-align: center;
`;

const StyledCreateListing = styled(UnstyledCreateListing)`
    margin-top: 4rem;
`;

import React, { FC } from 'react';
import { Base } from '@layouts/Base';
import { Container, Typography } from '@mui/material';
import Head from 'next/head';

export interface AboutProps {}

export type AboutType = FC<AboutProps>;

export const About: AboutType = () => {
    return (
        <Base>
            <Head>
                <title>About Dumpster Fire</title>
            </Head>

            <Container>
                <Typography variant="h4">
                    <p>About us</p>
                </Typography>

                <Typography variant="body1">
                    <p>
                        Dumpster fire is a metaphor for an extremely negative or
                        disastrous event. It typically refers to something that
                        goes wrong, rapidly and with great magnitude. The phrase
                        &quot;dumpster fire&quot; originates from the early
                        2000s in popular usage on social media as a way of
                        describing events such as natural disasters, political
                        scandals, tech company collapses and other large-scale
                        failures. The term has been spoofed by comedians into
                        various images depicting generally unsuccessful
                        situations (such as businesses going under or people
                        being humiliated).
                    </p>
                    <p>
                        Dumpster Fire is a brand new e-commerce platform that
                        sells anything that can be packaged. What this means for
                        you is that you can shop safely and securely with us,
                        whether you&apos;re looking for products for your home,
                        office, or lifestyle.
                    </p>
                    <p>
                        Our website is easy to use and navigate, making it
                        simple for you to find the products that you&apos;re
                        looking for. We offer a wide range of products,
                        including furniture, clothes, home accessories, and
                        more. In addition, we offer free shipping on all orders
                        over $50, so you can shop with confidence.
                    </p>
                    <p>
                        To shop with Dumpster Fire, simply select the items that
                        you wish to purchase, and enter your address. We will
                        then provide you with a shipping estimate, as well as
                        details about how to pay for your order. If you have any
                        questions about shopping with Dumpster Fire, please
                        don&apos;t hesitate to contact us. We would love to help
                        you find the products that you need.
                    </p>
                    <p>
                        Dumpster Fire is a new e-commerce platform that sells
                        anything that can be packaged. What this means for you
                        is that you can shop safely and securely with us,
                        whether you&apos;re looking for products for your home,
                        office, or lifestyle.Our website is easy to use and
                        navigate, making it simple for you to find the products
                        that you&apos;re looking for.
                    </p>
                </Typography>
            </Container>
        </Base>
    );
};

import { FC } from 'react';
import { Stack, Typography } from '@mui/material';
import NoResults from './noresults.svg';
import Image from 'next/image';

export const NotFound: FC = () => {
    return (
        <Stack
            width="100%"
            direction="column"
            justifyContent="center"
            alignItems="center"
            spacing={4}
        >
            <Image src={NoResults} alt="No results" width={400} height={400} />
            <Typography fontSize={32}>There is nothing here...</Typography>
        </Stack>
    );
};

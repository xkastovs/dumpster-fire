import { ListingGrid } from '@components/common/ListingGrid';
import { FC } from 'react';
import { Base } from '@layouts/Base';
import Head from 'next/head';
import {
    App_Listings_Bool_Exp,
    App_Listings_Order_By,
    InputMaybe,
    Order_By,
    useGetListingsQuery,
} from 'gql/generated/react';
import { Orderings, useSearch } from '@hooks/useSearch';
import { Box, Pagination } from '@mui/material';
import { PAGE_SIZE } from '@constants/PAGE_SIZE';
import { ListingFilters } from './ListingFilters';
import { NotFound } from './NotFound';
import { usePage } from '@hooks/usePage';

export interface HomeProps {}
export type HomeType = FC<HomeProps>;

const resolveFilter = ({
    query,
    maxPrice,
    minPrice,
}: ReturnType<typeof useSearch>): InputMaybe<App_Listings_Bool_Exp> => {
    if (!query && !maxPrice && !minPrice) return null;
    const filter: App_Listings_Bool_Exp = { _and: [] };

    if (query) filter._and?.push({ title: { _ilike: `%${query}%` } });
    if (maxPrice) filter._and?.push({ price: { _lte: maxPrice } });
    if (minPrice) filter._and?.push({ price: { _gte: minPrice } });
    return filter;
};

export const Home: HomeType = () => {
    const [page, setPage] = usePage();
    const search = useSearch();
    const { data } = useGetListingsQuery({
        variables: {
            limit: PAGE_SIZE,
            offset: (page - 1) * PAGE_SIZE,
            order_by: Orderings[search.ordering],
            filter: resolveFilter(search),
        },
    });

    const count = data?.app_listings_aggregate.aggregate?.count ?? 0;

    return (
        <Base>
            <Head>
                <title>Dumpster Fire - Home</title>
            </Head>
            <ListingFilters />

            {count === 0 ? (
                <NotFound />
            ) : (
                <ListingGrid listings={data?.app_listings} />
            )}
            <Box
                justifyContent={'center'}
                alignItems={'center'}
                display={'flex'}
                sx={{ margin: '20px 0px' }}
            >
                <Pagination
                    count={Math.ceil((count ?? 0) / PAGE_SIZE)}
                    onChange={(_, p) => setPage(p)}
                />
            </Box>
        </Base>
    );
};

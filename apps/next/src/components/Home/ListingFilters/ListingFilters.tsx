import { Orderings, useSearch } from '@hooks/useSearch';
import Search from '@mui/icons-material/Search';
import {
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    Stack,
    TextField,
} from '@mui/material';
import { Box } from '@mui/system';
import { FC, useCallback, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { PrimaryButton } from 'ui';

export interface ListingFiltersProps {}
export type ListingFiltersType = FC<ListingFiltersProps>;

export const ListingFilters: ListingFiltersType = () => {
    const {
        setMinPrice,
        setMaxPrice,
        setOrdering,
        setQuery,
        minPrice,
        maxPrice,
        query,
        ordering,
    } = useSearch();

    const { register, handleSubmit, reset } = useForm({
        defaultValues: {
            maxPrice,
            minPrice,
            query,
            ordering,
        },
    });

    useEffect(() => {
        reset({ maxPrice, minPrice, query });
    }, [maxPrice, minPrice, query, reset]);

    const onValidSubmit = useCallback<Parameters<typeof handleSubmit>[0]>(
        ({ maxPrice, minPrice, query, ordering }) => {
            // these calls don't get batched together, an optimization is possible
            setMinPrice(minPrice);
            setMaxPrice(maxPrice);
            setQuery(query);
            setOrdering(ordering);
        },
        [setMaxPrice, setMinPrice, setOrdering, setQuery]
    );

    return (
        <form onSubmit={handleSubmit(onValidSubmit)}>
            <Stack width="100%" spacing={2} mb={4}>
                <Box width="100%">
                    <TextField
                        fullWidth
                        InputProps={{ startAdornment: <Search /> }}
                        label="Title"
                        type="search"
                        {...register('query')}
                    ></TextField>
                </Box>
                <Stack
                    width="100%"
                    direction="row"
                    justifyContent="space-between"
                    spacing={2}
                >
                    <Stack
                        direction="row"
                        justifyContent="space-between"
                        width="100%"
                        spacing={2}
                    >
                        <Stack direction="row" spacing={2}>
                            <TextField
                                InputProps={{ startAdornment: ' ' }} // This prevents the label from collapsing in the field, but is kinda lidl
                                inputProps={{ min: 0 }}
                                type="number"
                                label="Min price"
                                {...register('minPrice')}
                            ></TextField>
                            <TextField
                                InputProps={{ startAdornment: ' ' }}
                                inputProps={{ min: 0 }}
                                type="number"
                                label="Max price"
                                {...register('maxPrice')}
                            ></TextField>
                        </Stack>
                        <FormControl>
                            <InputLabel id="order-by">Order by</InputLabel>
                            <Select
                                sx={{ minWidth: 200 }}
                                fullWidth
                                labelId='"order-by'
                                label="Order by"
                                inputProps={register('ordering')}
                            >
                                {Object.keys(Orderings).map((key) => (
                                    <MenuItem key={key} value={key}>
                                        {key}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Stack>
                    <PrimaryButton type="submit">Search</PrimaryButton>
                </Stack>
            </Stack>
        </form>
    );
};

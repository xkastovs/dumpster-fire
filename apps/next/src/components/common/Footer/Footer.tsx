import * as React from 'react';

import { FooterLogo } from '@components/common/Navbar/Logo';
import { FC, PropsWithChildren } from 'react';
import { Link, styled, Typography } from '@mui/material';

export interface FooterProps {}
export type FooterType = FC<PropsWithChildren<FooterProps>>;

const StyledFooter = styled('footer')`
    background: lightgray;
    margin-top: auto;
    padding: 0.5rem;

    display: flex;
    flex-direction: row;
    align-items: center;
`;

const CopyrightSpan = styled(Typography)`
    font-size: xx-small;
    margin-left: auto;
    margin-right: 1rem;
`;

const AboutSpan = styled(Typography)`
    font-size: x-small;
    font-family: sans-serif;
    margin-left: 1rem;
`;

export const Footer: FooterType = () => {
    return (
        <StyledFooter>
            <FooterLogo />

            <Link
                href="/about"
                sx={{ color: 'inherit', textDecoration: 'none' }}
            >
                <AboutSpan variant="caption">About</AboutSpan>
            </Link>

            <CopyrightSpan variant="caption">
                © Copyright Cannabis Smokers Inc. 2022
            </CopyrightSpan>
        </StyledFooter>
    );
};

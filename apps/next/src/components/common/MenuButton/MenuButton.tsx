import * as React from 'react';

import { FC } from 'react';
import Link from 'next/link';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';

export interface MenuButtonProps {
    text: string;
    path: string;
    onClick?: () => void;
}
export type MenuButtonType = FC<MenuButtonProps>;

export const DesktopMenuButton: MenuButtonType = ({ text, path }) => {
    return (
        <Link href={path} passHref>
            <Button
                key={text}
                sx={{
                    my: 2,
                    color: 'white',
                    display: 'block',
                }}
            >
                {text}
            </Button>
        </Link>
    );
};

export const MobileMenuButton: MenuButtonType = ({ text, path, onClick }) => {
    return (
        <Link href={path} passHref>
            <MenuItem key={text} onClick={onClick}>
                <Typography textAlign="center">{text}</Typography>
            </MenuItem>
        </Link>
    );
};

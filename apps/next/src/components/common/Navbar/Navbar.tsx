import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Container from '@mui/material/Container';
import Tooltip from '@mui/material/Tooltip';
import { theme_dark } from 'theme';
import { Logo } from './Logo';
import { styled } from '@mui/material';
import {
    DesktopNavMenuBox,
    MobileNavMenuBox,
} from '@components/common/Navbar/NavMenu';
import { HamburgerButton } from '@components/common/Navbar/HamburgerButton';
import { HamburgerMenu } from '@components/common/Navbar/HamburgerMenu';
import { UserHamburgerMenu } from '@components/common/Navbar/HamburgerMenu/UserHamburgerMenu';
import {
    UserMenuBox,
    UserMenuButton,
} from '@components/common/Navbar/UserMenu';
import { DesktopMenuButton, MobileMenuButton } from '../MenuButton';
import { AppShoppingCart } from './ShoppingCart';
import { SearchAppBar } from './Search';
import { paths } from '@routes/pages';

const StickyAppBar = styled(AppBar)`
    position: sticky;
    background-image: ${theme_dark.colors.primary};
`;

const ResponsiveAppBar = () => {
    const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(
        null
    );
    const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
        null
    );

    const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElNav(event.currentTarget);
    };
    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    return (
        <StickyAppBar>
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <Logo />

                    <MobileNavMenuBox>
                        <HamburgerButton onClick={handleOpenNavMenu} />
                        <HamburgerMenu
                            anchorEl={anchorElNav}
                            onClose={handleCloseNavMenu}
                        >
                            {Object.entries(paths).map(
                                ([pageName, pagePath]) => (
                                    <MobileMenuButton
                                        key={pagePath}
                                        text={pageName}
                                        path={pagePath}
                                        onClick={handleCloseNavMenu}
                                    />
                                )
                            )}
                        </HamburgerMenu>
                    </MobileNavMenuBox>

                    <DesktopNavMenuBox>
                        {/* {Object.entries(paths).map(([pageName, pagePath]) => (
                            <DesktopMenuButton
                                key={pagePath}
                                text={pageName}
                                path={pagePath}
                            />
                        ))} */}
                    </DesktopNavMenuBox>

                    <SearchAppBar />

                    <AppShoppingCart />

                    <UserMenuBox>
                        <Tooltip title="Open settings">
                            <UserMenuButton onClick={handleOpenUserMenu} />
                        </Tooltip>
                        <UserHamburgerMenu
                            anchorEl={anchorElUser}
                            onClose={handleCloseUserMenu}
                        />
                    </UserMenuBox>
                </Toolbar>
            </Container>
        </StickyAppBar>
    );
};

export default ResponsiveAppBar;

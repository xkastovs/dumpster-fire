import { styled, alpha } from '@mui/material/styles';
import Box from '@mui/material/Box';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import { FC, KeyboardEventHandler, useCallback } from 'react';
import { useSearch } from '@hooks/useSearch';

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 1,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(1),
        width: 'auto',
    },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
}));

export const SearchAppBar: FC = () => {
    const { setQuery } = useSearch();
    const onEnterPress = useCallback<KeyboardEventHandler<HTMLInputElement>>(
        (e) => {
            if (e.key != 'Enter') return;
            setQuery(e.currentTarget.value);
        },
        [setQuery]
    );

    return (
        <Box sx={{ flexGrow: 0, marginRight: '20px' }}>
            <Search>
                {/* seach icon */}
                <SearchIconWrapper>
                    <SearchIcon />
                </SearchIconWrapper>

                <StyledInputBase
                    onKeyDown={onEnterPress}
                    placeholder="Search..."
                    inputProps={{ 'aria-label': 'search' }}
                />
            </Search>
        </Box>
    );
};

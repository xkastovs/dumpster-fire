import * as React from 'react';

import { FC, PropsWithChildren } from 'react';
import Box from '@mui/material/Box';
import { styled } from '@mui/material';

export interface MenuProps {}
export type MenuType = FC<PropsWithChildren<MenuProps>>;

const FlexGrownBox = styled(Box)`
    flex-grow: 1;
`;

export const MobileNavMenuBox: MenuType = ({ children }) => {
    return (
        <FlexGrownBox sx={{ display: { xs: 'flex', md: 'none' } }}>
            {children}
        </FlexGrownBox>
    );
};

export const DesktopNavMenuBox: MenuType = ({ children }) => {
    return (
        <FlexGrownBox sx={{ display: { xs: 'none', md: 'flex' } }}>
            {children}
        </FlexGrownBox>
    );
};

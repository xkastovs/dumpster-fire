import * as React from 'react';

import {
    HamburgerMenuBase,
    HamburgerMenuType,
} from '@components/common/Navbar/HamburgerMenu/HamburgerMenuBase';

export const HamburgerMenu: HamburgerMenuType = ({
    anchorEl,
    onClose,
    children,
}) => {
    return (
        <HamburgerMenuBase
            anchorEl={anchorEl}
            onClose={onClose}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
            }}
            sx={{
                display: { xs: 'block', md: 'none' },
            }}
        >
            {children}
        </HamburgerMenuBase>
    );
};

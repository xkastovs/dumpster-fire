import * as React from 'react';

import {
    HamburgerMenuBase,
    HamburgerMenuType,
} from '@components/common/Navbar/HamburgerMenu/HamburgerMenuBase';
import { MobileNavMenuButton } from '@components/common/Navbar/NavMenuButton';
import { useSessionContext } from 'supertokens-auth-react/recipe/session';
import { user_pages } from '@routes/user_pages';

export const UserHamburgerMenu: HamburgerMenuType = ({ anchorEl, onClose }) => {
    const { doesSessionExist, userId } = useSessionContext();

    return (
        <HamburgerMenuBase
            anchorEl={anchorEl}
            onClose={onClose}
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            sx={{ mt: '45px' }}
        >
            {doesSessionExist ? (
                Object.entries(user_pages).map(([pageName, pagePath]) => (
                    <MobileNavMenuButton
                        key={pagePath}
                        text={pageName}
                        path={`${pagePath.replace('${userid}', userId)}`}
                        onClick={onClose}
                    />
                ))
            ) : (
                <MobileNavMenuButton
                    key={'auth'}
                    text={'Sign in / Sign up'}
                    path={'/auth'}
                    onClick={onClose}
                />
            )}
        </HamburgerMenuBase>
    );
};

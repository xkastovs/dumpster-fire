import * as React from 'react';

import { FC, PropsWithChildren } from 'react';
import Menu from '@mui/material/Menu';
import { PopoverOrigin, SxProps, Theme } from '@mui/material';

export interface MenuProps {
    anchorEl: HTMLElement | null;
    onClose: (event: React.MouseEvent<HTMLElement>) => void;
}
export interface InnerMenuProps extends MenuProps {
    anchorOrigin?: PopoverOrigin;
    transformOrigin?: PopoverOrigin;
    sx?: SxProps<Theme>;
}

export type HamburgerMenuBaseType = FC<PropsWithChildren<InnerMenuProps>>;
export type HamburgerMenuType = FC<PropsWithChildren<MenuProps>>;

export const HamburgerMenuBase: HamburgerMenuBaseType = ({
    anchorEl,
    onClose,
    children,
    anchorOrigin,
    transformOrigin,
    sx,
}) => {
    return (
        <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            keepMounted
            anchorOrigin={anchorOrigin}
            transformOrigin={transformOrigin}
            open={Boolean(anchorEl)}
            onClose={onClose}
            sx={sx}
        >
            {children}
        </Menu>
    );
};

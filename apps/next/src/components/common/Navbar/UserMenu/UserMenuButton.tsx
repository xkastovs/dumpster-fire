import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import * as React from 'react';
import { forwardRef } from 'react';
import { styled } from '@mui/material';

export interface UserMenuButtonProps {
    onClick: React.MouseEventHandler;
}

const PaddingLessButton = styled(IconButton)`
    padding: 0;
`;

export const UserMenuButton = forwardRef<
    HTMLButtonElement,
    UserMenuButtonProps
>((props, ref) => {
    return (
        <PaddingLessButton ref={ref} {...props}>
            <Avatar />
        </PaddingLessButton>
    );
});

import * as React from 'react';

import { FC, PropsWithChildren } from 'react';
import Box from '@mui/material/Box';
import { styled } from '@mui/material';

export interface UserMenuProps {}
export type MenuType = FC<PropsWithChildren<UserMenuProps>>;

const FlexShrunkBox = styled(Box)`
    flex-grow: 0;
`;

export const UserMenuBox: MenuType = ({ children }) => {
    return <FlexShrunkBox>{children}</FlexShrunkBox>;
};

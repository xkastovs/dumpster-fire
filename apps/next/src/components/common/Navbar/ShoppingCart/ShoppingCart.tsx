import { ShoppingCart } from '@mui/icons-material';
import { Button, ListItemIcon, Tooltip } from '@mui/material';

export const AppShoppingCart = () => {
    return (
        <Tooltip title="Shopping cart">
            <Button color="secondary">
                <ListItemIcon>
                    <ShoppingCart />
                </ListItemIcon>
            </Button>
        </Tooltip>
    );
};

import React, { ComponentType } from 'react';
import Typography, { TypographyProps } from '@mui/material/Typography';
import { Link, styled } from '@mui/material';

const StyledTypography = styled(Typography)`
    ::before {
        content: '🔥';
        margin-right: 0.5rem;
    }
    margin-right: 2px;
    display: flex;
    font-family: monospace;
    font-weight: 800;
    letter-spacing: 0.32rem;
    color: inherit;
    text-decoration: none;
`;

const FooterTypography = styled(StyledTypography)`
    filter: grayscale(0.75);
    font-size: 0.75rem;
`;

const withInnerLogo = <T extends ComponentType<TypographyProps>>(
    Component: T
) => {
    const ComponentCast = Component as any;

    return () => (
        <Link href="/" sx={{ color: 'inherit', textDecoration: 'none' }}>
            <ComponentCast variant="h6" noWrap>
                DUMPSTER FIRE
            </ComponentCast>
        </Link>
    );
};

export const Logo = withInnerLogo(StyledTypography);
export const FooterLogo = withInnerLogo(FooterTypography);

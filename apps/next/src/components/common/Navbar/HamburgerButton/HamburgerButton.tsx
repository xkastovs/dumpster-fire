import * as React from 'react';

import { FC } from 'react';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

export interface HamburgerButtonProps {
    onClick: (event: React.MouseEvent<HTMLElement>) => void;
}
export type HamburgerButtonType = FC<HamburgerButtonProps>;

export const HamburgerButton: HamburgerButtonType = ({ onClick }) => {
    return (
        <IconButton
            size="large"
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={onClick}
            color="inherit"
        >
            <MenuIcon />
        </IconButton>
    );
};

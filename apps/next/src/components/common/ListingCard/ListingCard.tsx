import { List_ListingFragment } from 'gql/generated/react';
import Link from 'next/link';
import { title } from 'process';
import React, { FC } from 'react';
import { theme_dark } from 'theme';
import { Card, Stack } from 'ui';
import { CardImage } from './CardImage';
import { CardPrice } from './CardPrice';
import { CardTitle } from './CardTitle';

export type ListingCardProps = {
    listing: List_ListingFragment;
};
export type ListingCardType = FC<ListingCardProps>;

export const ListingCard: ListingCardType = ({ listing }) => {
    const image =
        listing.thumbnail?.link ??
        'https://i.pinimg.com/originals/0c/5c/82/0c5c82f69c8516e26deac3258a9bfcd6.png';

    return (
        <Link href={`http://localhost:3000/listing/${listing.id}`}>
            <Card
                sx={{
                    '&:hover': {
                        boxShadow: theme_dark.shadows.cardHover,
                        backgroundImage: theme_dark.colors.primary,
                        cursor: 'pointer',
                    },
                }}
            >
                <Stack direction={'column'} alignItems={'stretch'}>
                    <CardImage src={image} alt={title} />
                    <Stack direction={'column'} margin="10px" gap="0.75rem">
                        <CardTitle>{listing.title}</CardTitle>
                        <CardPrice
                            price={listing.price}
                            currency={listing.currency}
                        ></CardPrice>
                    </Stack>
                </Stack>
            </Card>
        </Link>
    );
};

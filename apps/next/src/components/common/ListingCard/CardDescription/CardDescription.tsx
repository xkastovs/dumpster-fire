import { FC } from 'react';
export interface CardDescriptionProps {
    description: string | null | undefined;
}

export const CardDescription: FC<CardDescriptionProps> = ({ description }) => {
    return <div style={{ textOverflow: 'ellipsis' }}>{description}</div>;
};

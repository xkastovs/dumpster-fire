import Image from 'next/image';
import { FC } from 'react';

export interface CardImageProps {
    src: string;
    alt: string;
}

export const CardImage: FC<CardImageProps> = ({ src, alt }) => {
    return <Image src={src} alt={alt} width="200px" height="200px" />;
};

import { styled } from '@mui/material';

export const CardTitle = styled('div')`
    font-size: 1rem;
    font-weight: 400;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;

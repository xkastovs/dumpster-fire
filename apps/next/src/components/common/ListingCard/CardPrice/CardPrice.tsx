import { styled } from '@mui/material';
import { FC } from 'react';

export interface PriceProps {
    price: number;
    currency: string;
}

const StyledPrice = styled('div')`
    font-weight: 600;
    /* margin-bottom: 5px; */
`;

export const CardPrice: FC<PriceProps> = ({ price, currency }) => {
    return (
        <StyledPrice>
            {new Intl.NumberFormat(undefined, {
                currency,
                style: 'currency',
            }).format(price)}
        </StyledPrice>
    );
};

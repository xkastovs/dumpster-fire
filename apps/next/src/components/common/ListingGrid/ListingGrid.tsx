import { List_ListingFragment } from 'gql/generated/react';
import React, { FC } from 'react';
import { Grid } from 'ui';
import { ListingCard } from '../ListingCard';

export type ListingGridProps = {
    listings: List_ListingFragment[] | undefined;
};

export const ListingGrid: FC<ListingGridProps> = ({ listings }) => {
    return (
        <Grid container direction={'row'} spacing={3} justifyContent={'center'}>
            {listings?.map((listing) => (
                <Grid item key={listing.id}>
                    <ListingCard listing={listing} />
                </Grid>
            ))}
        </Grid>
    );
};

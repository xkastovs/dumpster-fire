import React from 'react';
import { Select } from '@mui/material';
import { Controller, FieldValues } from 'react-hook-form';
import { RadioProps } from '@components/common/forms/FormRadioGroup';
import MenuItem from '@mui/material/MenuItem';
import Tooltip from '@mui/material/Tooltip';

export const FormDropdownSelect = <T extends FieldValues>({
    name,
    control,
    label,
    options,
}: RadioProps<T>) => {
    const generateMenuItems = () => {
        return options.map((singleOption) => (
            <MenuItem key={singleOption.value} value={singleOption.value}>
                {singleOption.label}
            </MenuItem>
        ));
    };

    return (
        <Controller
            name={name}
            control={control}
            render={({ field: { onChange, value } }) => (
                <Tooltip title={label} placement={'top'}>
                    <Select
                        value={value ?? ''}
                        onChange={onChange}
                        sx={{ height: 1 }}
                    >
                        {generateMenuItems()}
                    </Select>
                </Tooltip>
            )}
        />
    );
};

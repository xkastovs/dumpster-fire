import { styled } from '@mui/material';
import Box from '@mui/material/Box';

export const OutlinedBox = styled(Box)`
    border: 1px #ccc solid;
    border-radius: 0.25rem;
    margin: 0;
    padding-bottom: 0;
`;

export const LeftPaddedOutlinedBox = styled(OutlinedBox)`
    padding: 0 1rem 1rem 1rem;
`;

import React from 'react';
import { FormControlLabel, FormLabel, Radio, RadioGroup } from '@mui/material';
import { Controller, FieldValues } from 'react-hook-form';
import { OutlinedBox } from '@components/common/forms/OutlinedBox';
import { ListingFormInputProps } from '@typings/FormInputProps';

export type LabelValuePair = {
    label: string;
    value: string;
};

export interface RadioProps<T> extends ListingFormInputProps<T> {
    options: Array<LabelValuePair>;
}

export const FormRadioGroup = <T extends FieldValues>({
    name,
    control,
    label,
    options,
}: RadioProps<T>) => {
    const generateRadioOptions = () => {
        return options.map((singleOption) => (
            <FormControlLabel
                key={singleOption.value}
                value={singleOption.value}
                label={singleOption.label}
                control={<Radio />}
            />
        ));
    };

    return (
        <Controller
            name={name}
            control={control}
            render={({ field: { onChange, value } }) => (
                <OutlinedBox component="fieldset">
                    <FormLabel>{label}</FormLabel>
                    <RadioGroup value={value ?? ' '} onChange={onChange} row>
                        {generateRadioOptions()}
                    </RadioGroup>
                </OutlinedBox>
            )}
        />
    );
};

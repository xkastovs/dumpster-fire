import React from 'react';
import { Controller, FieldValues } from 'react-hook-form';
import { styled, TextareaAutosize } from '@mui/material';
import { ListingFormInputProps } from '@typings/FormInputProps';

const PaddedTextArea = styled(TextareaAutosize)`
    padding: 0.5rem;
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    resize: vertical;
    border: 1px #ccc solid;
    border-radius: 0.25rem;
    min-height: 2rem;
`;

export const FormTextArea = <T extends FieldValues>({
    control,
    name,
    label,
}: ListingFormInputProps<T>) => {
    return (
        <Controller
            name={name}
            control={control}
            render={({ field: { onChange, value } }) => (
                <PaddedTextArea
                    minRows={3}
                    onChange={onChange}
                    value={value ?? ''}
                    placeholder={label}
                />
            )}
        />
    );
};

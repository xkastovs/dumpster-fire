import TextField from '@mui/material/TextField';
import React from 'react';
import { Controller, FieldValues } from 'react-hook-form';
import { ListingFormInputProps } from '@typings/FormInputProps';

export const FormTextInput = <T extends FieldValues>({
    control,
    name,
    label,
    type,
}: ListingFormInputProps<T>) => {
    return (
        <Controller
            name={name}
            control={control}
            render={({ field: { onChange, value } }) => (
                <TextField
                    fullWidth
                    onChange={onChange}
                    value={value ?? ''}
                    label={label}
                    type={type}
                />
            )}
        />
    );
};

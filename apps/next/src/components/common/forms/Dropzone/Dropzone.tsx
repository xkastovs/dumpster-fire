import React, { FC } from 'react';
import { useDropzone, DropzoneOptions } from 'react-dropzone';
import { Stack } from 'ui';
import { Alert, Fade, styled } from '@mui/material';
import Tooltip from '@mui/material/Tooltip';
import { DropzoneFileCard } from '@components/common/forms/Dropzone/DropzoneFileCard';
import { LeftPaddedOutlinedBox } from '@components/common/forms/OutlinedBox';
import { UploadingFile } from '@hooks/useDropzoneFiles';

export interface DropzoneProps
    extends Pick<DropzoneOptions, 'onDrop' | 'onDropRejected'> {
    files: UploadingFile[];
    errors: JSX.Element[] | null;
    clearFiles: (toBeDeleted: UploadingFile) => void;
    clearErrors: () => void;
}

export const Dropzone: FC<DropzoneProps> = ({
    files,
    onDrop,
    onDropRejected,
    errors,
    clearFiles,
    clearErrors,
}) => {
    const { getRootProps, getInputProps } = useDropzone({
        onDrop,
        onDropRejected,
        multiple: true,
        accept: {
            'image/jpeg': [],
            'image/png': [],
            'image/gif': [],
        },
        maxSize: 2 * 1024 * 1024,
    });

    return (
        <LeftPaddedOutlinedBox {...getRootProps()}>
            <Tooltip title="Click to select images to upload, or drag and drop them here.">
                <div>
                    <div>
                        <input {...getInputProps()} />
                        <p>Images</p>
                    </div>
                    <aside>
                        <Stack gap={'1rem'}>
                            {files.map((f) => (
                                <DropzoneFileCard
                                    file={f}
                                    deleteFunction={clearFiles}
                                />
                            ))}
                        </Stack>
                    </aside>
                </div>
            </Tooltip>

            <MarginedFade in={errors != null} unmountOnExit>
                <Alert
                    severity="error"
                    onClose={(event) => {
                        event.stopPropagation();
                        clearErrors();
                    }}
                >
                    {errors}
                </Alert>
            </MarginedFade>
        </LeftPaddedOutlinedBox>
    );
};

const MarginedFade = styled(Fade)`
    margin-top: 1rem;
`;

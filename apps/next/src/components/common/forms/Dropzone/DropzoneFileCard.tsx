import Typography from '@mui/material/Typography';
import { CardActions, styled } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import { Delete } from '@mui/icons-material';
import { Card } from 'ui';
import React, { FC } from 'react';
import { UploadingFile } from '@hooks/useDropzoneFiles';

export interface DropzoneFileCardProps {
    file: UploadingFile;
    deleteFunction: (toBeDeleted: UploadingFile) => void;
}
export type DropzoneFileCardType = FC<DropzoneFileCardProps>;

export const DropzoneFileCard: DropzoneFileCardType = ({
    file: uploadingFile,
    deleteFunction,
}) => {
    return (
        <StyledDropzoneCard>
            <EllipsisTypography sx={{ flexGrow: 1 }}>
                {uploadingFile.file.name} -{' '}
                {formatBytes(uploadingFile.file.size)}
            </EllipsisTypography>
            <CardActions sx={{ flexGrow: 0 }}>
                <CardDeleteButton
                    file={uploadingFile}
                    deleteFunction={deleteFunction}
                />
            </CardActions>
        </StyledDropzoneCard>
    );
};

const EllipsisTypography = styled(Typography)`
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;

const StyledDropzoneCard = styled(Card)`
    display: flex;
    flex-direction: row;
    align-items: center;
    padding-left: 0.5rem;
`;

const formatBytes = (bytes: number, decimals = 2) => {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
};

interface DropzoneFileDeleteButtonProps extends DropzoneFileCardProps {}
type DropzoneFileDeleteButtonType = FC<DropzoneFileDeleteButtonProps>;

const CardDeleteButton: DropzoneFileDeleteButtonType = ({
    file,
    deleteFunction,
}) => {
    return (
        <IconButton
            size="small"
            onClick={(event) => {
                event.stopPropagation();
                deleteFunction(file);
            }}
        >
            <Delete fontSize="inherit" />
        </IconButton>
    );
};

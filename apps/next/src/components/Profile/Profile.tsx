import { useGetUserProfileQuery } from 'gql/generated/react';
import React, { FC, useEffect } from 'react';
import { Base } from 'src/layouts/Base';
import { useRouter } from '@hooks/useRouter';
import { Skeleton, Stack, Typography } from '@mui/material';
import { _404 } from '@components/404';
import Head from 'next/head';
import { ListingGrid } from '@components/common/ListingGrid';
import { Card, Grid } from 'ui';
import { theme_dark } from 'theme';
import { EditableText } from '@components/EditTextButton';
import { useUpdateUser } from '@hooks/useUpdateUser';

export interface ProfileProps {}
export type ProfileType = FC<ProfileProps>;

export const Profile: ProfileType = () => {
    const router = useRouter();
    const id = router.query.id as string;
    const { data, error, loading } = useGetUserProfileQuery({
        variables: { id },
        skip: !id,
    });

    const { user, setUser, onNameEdit } = useUpdateUser();

    useEffect(() => {
        const fetchData = () => {
            setUser(data?.app_users_by_pk!);
        };
        fetchData();
    }, [data, setUser]);

    if (loading) {
        return (
            <Base>
                <Skeleton variant={'rectangular'} />
            </Base>
        );
    }

    if (error) {
        return <_404 errorMessage={'User not found'} />;
    }

    // TODO: create user profile view
    return (
        <Base>
            <Head>
                <title>{user?.name ?? 'User'}&apos;s profile</title>
            </Head>

            <Grid direction={'column'} maxWidth={'xl'} gap="2rem" container>
                <Grid item xs={12}>
                    <Card>
                        <Stack
                            fontSize={25}
                            direction={'column'}
                            gap={'1rem'}
                            margin={theme_dark.margins.defaultCard}
                        >
                            <Typography fontWeight={1} fontSize={42}>
                                Profile
                            </Typography>
                            <Stack direction={'row'}>
                                <EditableText
                                    value={user?.name}
                                    handleEdit={onNameEdit}
                                    editLabel="Change name"
                                />
                            </Stack>
                            <div>Email: {user?.email}</div>
                            <div>
                                Open listings: {user?.listings?.keys.length}
                            </div>
                        </Stack>
                    </Card>
                </Grid>
                <Grid item xs={12} marginTop="2rem">
                    <Typography fontWeight={1} textAlign="center" fontSize={42}>
                        My listings
                    </Typography>
                    <ListingGrid listings={user?.listings}></ListingGrid>
                </Grid>
            </Grid>
        </Base>
    );
};

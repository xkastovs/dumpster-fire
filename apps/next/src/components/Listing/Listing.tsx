import { CardPrice } from '@components/common/ListingCard/CardPrice';
import { useRouter } from '@hooks/useRouter';
import { Base } from '@layouts/Base';
import { Divider, Link, Typography } from '@mui/material';
import {
    Detailed_ListingFragment,
    useGetListingQuery,
    useGetListingsQuery,
    useGetUserProfileQuery,
} from 'gql/generated/react';
import Image from 'next/image';
import Carousel from 'nuka-carousel';
import { _404 } from '@components/404';
import React, { FC, PropsWithChildren } from 'react';
import { Card, Container, Grid, Stack } from 'ui';
import Head from 'next/head';
import { ListingGrid } from '@components/common/ListingGrid';
import { IMAGE_BASE_URL } from '@constants/IMAGE_BASE_URL';

export interface ListingProps {
    listing: Detailed_ListingFragment;
}
export const Listing: FC<ListingProps> = () => {
    const router = useRouter();
    const id = router.query.id as string;

    const randomNumber = (min: number, max: number) => {
        return Math.floor(Math.random() * (max - min) + min);
    };
    const randomListings = useGetListingsQuery({
        variables: {
            filter: {},
            limit: 10,
            offset: randomNumber(0, 90),
            order_by: {},
        },
    }).data?.app_listings;

    const response = useGetListingQuery({ variables: { id }, skip: !id });
    const listing = response?.data?.app_listings_by_pk;

    const photos = listing?.photos;
    console.log(photos);
    const user = useGetUserProfileQuery({ variables: listing?.creator })?.data
        ?.app_users_by_pk;

    if (!listing) {
        return <_404 errorMessage={'Listing was not found'} />;
    }

    if (!user) {
        return <_404 errorMessage={'Listing author was not found'} />;
    }

    const LoadPhotos = () => {
        const defaultImageSrc =
            'https://i.pinimg.com/originals/0c/5c/82/0c5c82f69c8516e26deac3258a9bfcd6.png';

        if (listing?.photos.length === 0) {
            return (
                <Image
                    src={defaultImageSrc}
                    alt={'default image'}
                    width={500}
                    height={500}
                />
            );
        }

        return photos?.map((photo) => (
            <Image
                src={`${IMAGE_BASE_URL}${photo.link}`}
                alt={photo.id}
                width={500}
                height={500}
            />
        ));
    };

    interface CarouselProps {}
    type BaseType = FC<PropsWithChildren<CarouselProps>>;

    const CarouselEffect: BaseType = ({ children }) => {
        return (
            <Carousel
                wrapAround={true}
                autoplay={true}
                autoplayInterval={5000}
                enableKeyboardControls={true}
                animation={'zoom'}
                zoomScale={0.6}
            >
                {children}
            </Carousel>
        );
    };

    return (
        <Base>
            <Head>
                <title>{listing.title}</title>
            </Head>

            <Container sx={{ marginTop: '3rem' }}>
                <Card>
                    <Grid container direction={'row'}>
                        <Grid item xs={5}>
                            {listing?.photos.length > 1 ? (
                                <CarouselEffect>{LoadPhotos()}</CarouselEffect>
                            ) : (
                                LoadPhotos()
                            )}
                        </Grid>

                        <Divider flexItem={true} orientation="vertical" />

                        <Grid item xs={5}>
                            <Stack
                                direction={'column'}
                                sx={{ margin: '10%' }}
                                gap={'2rem'}
                            >
                                <Typography variant="h4">
                                    {listing.title}
                                </Typography>

                                <Typography variant="body1">
                                    {listing?.description}
                                </Typography>

                                <CardPrice
                                    price={listing.price}
                                    currency={listing.currency}
                                ></CardPrice>

                                <Stack direction={'column'}>
                                    <Link
                                        href={`http://localhost:3000/user/${user.id!}`}
                                        sx={{
                                            color: 'inherit',
                                            textDecoration: 'none',
                                        }}
                                    >
                                        <Typography
                                            sx={{ textDecoration: 'underline' }}
                                        >
                                            {user.name}
                                        </Typography>
                                        <Typography>{user.email}</Typography>
                                    </Link>
                                </Stack>
                            </Stack>
                        </Grid>
                    </Grid>
                </Card>
            </Container>
            <Grid item xs={12} marginTop="2rem">
                <Typography fontWeight={1} textAlign="center" fontSize={42}>
                    Similar listings
                </Typography>
                <ListingGrid listings={randomListings}></ListingGrid>
            </Grid>
        </Base>
    );
};

import React, { FC, useCallback } from 'react';
import DoneIcon from '@mui/icons-material/Done';
import CloseIcon from '@mui/icons-material/Close';
import { FormTextInput } from '@components/common/forms/FormTextInput';
import { useForm } from 'react-hook-form';
import { Stack } from '@mui/material';

interface editResult {
    edited: string;
}

export interface EditTextProps {
    label: string;
    setVisible: (visible: boolean) => void;
    handleSuccess: (value: string) => void;
}

export const EditText: FC<EditTextProps> = ({
    label,
    setVisible,
    handleSuccess,
}) => {
    const { handleSubmit, control, getValues } = useForm();

    const handleAccept = useCallback(
        async (data: editResult) => {
            setVisible(false);
            console.log(data.edited);
            handleSuccess(data.edited);
        },
        [handleSuccess, setVisible]
    );

    return (
        <form>
            <Stack direction={'row'} gap={'1rem'} alignItems={'center'}>
                <FormTextInput
                    control={control}
                    label={label}
                    name="edited"
                    key="edited"
                />
                <DoneIcon
                    sx={{ cursor: 'pointer' }}
                    fontSize={'large'}
                    onClick={handleSubmit(() =>
                        handleAccept({ edited: getValues('edited') })
                    )}
                />
                <CloseIcon
                    sx={{ cursor: 'pointer' }}
                    fontSize={'large'}
                    onClick={() => setVisible(false)}
                />
            </Stack>
        </form>
    );
};

import React, { FC, useEffect } from 'react';
import { signOut } from 'supertokens-auth-react/lib/build/recipe/thirdpartyemailpassword';
import { useRouter } from '@hooks/useRouter';

export interface LogoutProps {}

export type LogoutType = FC<LogoutProps>;

export const Logout: LogoutType = () => {
    const { replace } = useRouter();

    useEffect(() => {
        const handleSignout = async () => {
            await signOut();
            return replace('/');
        };
        handleSignout();
    }, [replace]);

    return null;
};

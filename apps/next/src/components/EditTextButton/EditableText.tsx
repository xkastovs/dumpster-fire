import { EditText } from '@components/EditText/EditText';
import React, { FC, useState } from 'react';
import BorderColorIcon from '@mui/icons-material/BorderColor';

export interface EditTextButtonProps {
    value: string | null | undefined;
    handleEdit: (value: string) => void;
    editLabel: string;
}

export const EditableText: FC<EditTextButtonProps> = ({
    handleEdit,
    editLabel,
    value,
}) => {
    const [editing, setEditing] = useState(false);

    return (
        <>
            {editing ? (
                <EditText
                    handleSuccess={handleEdit}
                    setVisible={setEditing}
                    label={editLabel}
                />
            ) : (
                <div onClick={() => setEditing(true)}>
                    {value}
                    <BorderColorIcon
                        sx={{ cursor: 'pointer', marginLeft: '1rem' }}
                    />
                </div>
            )}
        </>
    );
};

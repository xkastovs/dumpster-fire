import React, { FC } from 'react';
import { Base } from '@layouts/Base';
import Head from 'next/head';
import Container from '@mui/material/Container';
import { Link } from '@mui/material';

export interface _404Props {
    errorMessage: string;
}

export type _404Type = FC<_404Props>;

export const _404: _404Type = ({ errorMessage }) => {
    return (
        <Base>
            <Head>
                <title>Something went wrong!</title>
            </Head>

            <Container>
                <h1>{errorMessage}</h1>

                <p>
                    Would you like to return&nbsp;
                    <Link href={'/'}>home?</Link>
                </p>
            </Container>
        </Base>
    );
};

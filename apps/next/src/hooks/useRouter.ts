import router, { NextRouter, useRouter as useNextRouter } from 'next/router';
import { isBrowser } from '@utils/isBrowser';

export const useRouter = (): NextRouter => {
    const nextRouter = useNextRouter();

    if (isBrowser()) return router;
    return nextRouter;
};

import React, { useCallback, useState } from 'react';
import {
    User_ProfileFragment,
    useUpdateUserProfileMutation,
} from 'gql/generated/react';

export const useUpdateUser = () => {
    const [user, setUser] = useState<User_ProfileFragment | undefined>();
    const [updateUserProfile] = useUpdateUserProfileMutation();

    const onNameEdit = useCallback(
        async (newName: string) => {
            setUser({ ...user!, name: newName });

            await updateUserProfile({
                variables: {
                    id: { id: user!.id },
                    input: { name: newName },
                },
            });
        },
        [user, updateUserProfile, setUser]
    );

    return { user, setUser, onNameEdit };
};

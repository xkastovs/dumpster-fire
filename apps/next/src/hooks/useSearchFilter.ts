import {
    RouteQueryOptions,
    TransitionOptions,
    UseRouterQueryParams,
    useRouterQueryParams,
} from './useRouterQueryParams';

export const useSearchFilter = <T>(
    key: string,
    options?: RouteQueryOptions<T>,
    transitionOptions?: TransitionOptions
): UseRouterQueryParams<T> =>
    useRouterQueryParams<T>(
        key,
        {
            method: 'push',
            ...options,
        },
        {
            shallow: true,
            ...transitionOptions,
        }
    );

import { useSearchFilter } from '@hooks/useSearchFilter';
import { Dispatch, useCallback } from 'react';

export const usePage = () => {
    const [page, _setPage, ...rest] = useSearchFilter<string>('page');
    const pageNum = page ? +page : 1;

    // We will not support functional updates here atm
    const setPage = useCallback<Dispatch<number>>(
        (page) => _setPage(`${page}`),
        [_setPage]
    );

    return [pageNum, setPage, ...rest] as const;
};

import { useRouter } from '@hooks/useRouter';
import { Dispatch, SetStateAction, useCallback } from 'react';
import { UrlObject } from 'url';
import { ParsedUrlQuery } from 'querystring';
import { log } from 'console';

type Method = 'push' | 'replace';

export type TransitionOptions = {
    shallow?: boolean;
    locale?: string | false;
    scroll?: boolean;
};

export type UseRouterQueryParams<T> = [
    param: T,
    setParam: Dispatch<SetStateAction<T>>,
    options: {
        removeParam: () => void;
    }
];

export type RouteQueryOptions<T> = {
    method?: Method;
    onSet?: (value: T, query: ParsedUrlQuery) => ParsedUrlQuery;
} & UrlObject;

export const useRouterQueryParams = <T = string>(
    key: string,
    options?: RouteQueryOptions<T>,
    transitionOptions?: TransitionOptions
): UseRouterQueryParams<T> => {
    const { query, push, replace, pathname } = useRouter();
    const queryStringRemoved = pathname.split(`?`)[0];
    const method = options?.method || 'push';
    const path = options?.pathname || queryStringRemoved;
    const routerMethod = method === 'push' ? push : replace;

    const removeOrSetParam = useCallback(
        (value: T) =>
            !value
                ? delete query[key]
                : void (query[key] = value as unknown as string), // lol
        [key, query]
    );

    const setParam: Dispatch<SetStateAction<T>> = useCallback(
        (value) => {
            if (isFunction(value))
                removeOrSetParam(value(query[key] as unknown as T)); // lol
            else removeOrSetParam(value);
            const newQuery = options?.onSet?.(value as T, query) || {
                ...query,
            };
            return routerMethod(
                {
                    ...options,
                    pathname: path,
                    query: newQuery,
                },
                undefined,
                transitionOptions
            ).catch(console.error);
        },
        [
            key,
            options,
            path,
            query,
            removeOrSetParam,
            routerMethod,
            transitionOptions,
        ]
    );

    const removeParam = useCallback(
        () => setParam(null as unknown as T), // lol
        [setParam]
    );

    return [query[key] as unknown as T, setParam, { removeParam }];
};

const isFunction = (value: any): value is Function =>
    typeof value === 'function';

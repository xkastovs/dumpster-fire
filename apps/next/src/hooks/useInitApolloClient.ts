import { useEffect, useState } from 'react';
import { NormalizedCacheObject } from '@apollo/client';
import { initializeApollo } from '@graphql/client/initializeApollo';
import { useSessionContext } from 'supertokens-auth-react/recipe/session';

export const useInitApolloClient = (initialState: NormalizedCacheObject) => {
    // accessTokenPayload is polymorphic depending on the authentication method, there is no way we can detect the type of the payload
    // we will be using client side fetching for client data, might migrate to server side rendering in the future,
    // but `initializeApollo` makes both variants interchangeable for the rest of the app
    const { accessTokenPayload } = useSessionContext();
    const [client, _setClient] = useState(() =>
        initializeApollo(initialState, accessTokenPayload?.jwt)
    );

    useEffect(() => {
        if (accessTokenPayload?.jwt) {
            _setClient(initializeApollo(initialState, accessTokenPayload.jwt));
        }
    }, [accessTokenPayload, initialState]);

    return client;
};

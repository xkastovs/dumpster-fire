import { useEffect } from 'react';
import { useRouter } from '@hooks/useRouter';
import { start, done } from 'nprogress';
import { MittEmitter } from 'next/dist/shared/lib/mitt';

export const useNProgress = () => {
    const { events } = useRouter();
    useEffect(() => {
        const onStart: MittEmitter<any>['on'] = (_, { shallow }: any) => {
            if (!shallow) start();
        };
        const onComplete: MittEmitter<any>['on'] = () => {
            done();
        };

        events.on('routeChangeStart', onStart);
        events.on('routeChangeComplete', onComplete);
        events.on('routeChangeError', onComplete);

        return () => {
            events.off('routeChangeStart', onStart);
            events.off('routeChangeComplete', onComplete);
            events.off('routeChangeError', onComplete);
        };
    }, [events]);
};

import React, { useCallback } from 'react';
import {
    Form_ListingFragment,
    useUpsertFormListingMutation,
} from 'gql/generated/react';
import { useSessionContext } from 'supertokens-auth-react/recipe/session';
import { UseFormGetValues } from 'react-hook-form';
import { useRouter } from '@hooks/useRouter';
import { UploadingFile } from './useDropzoneFiles';

export const useFormUpsertListing = (
    files: UploadingFile[],
    getFormValues: UseFormGetValues<Form_ListingFragment>
) => {
    const [upsertFormListing] = useUpsertFormListingMutation();
    const { userId } = useSessionContext();
    const { push } = useRouter();

    const onSubmit = useCallback(async () => {
        await upsertFormListing({
            variables: {
                input: {
                    title: getFormValues('title'),
                    description: getFormValues('description'),
                    price: getFormValues('price'),
                    currency: getFormValues('currency'),
                    creator_id: userId,
                    photos: {
                        data: files.map((file) => ({
                            link: file.resourceUrl,
                        })),
                    },
                },
            },
        });

        await push('/');
    }, [upsertFormListing, getFormValues, userId, files, push]);

    return { onSubmit };
};

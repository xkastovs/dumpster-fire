import { useSearchFilter } from '@hooks/useSearchFilter';
import { App_Listings_Order_By, Order_By } from 'gql/generated/react';
import { useCallback } from 'react';
import { usePage } from './usePage';
import { RouteQueryOptions } from './useRouterQueryParams';

export const Orderings = {
    Newest: { created_at: Order_By.Desc } as App_Listings_Order_By,
    Cheapest: { price: Order_By.Asc } as App_Listings_Order_By,
    'Most expensive': { price: Order_By.Desc } as App_Listings_Order_By,
} as const;

export type OrderingsType = typeof Orderings;

export const useSearch = () => {
    const [, , { removeParam }] = usePage();

    const onSet = useCallback<NonNullable<RouteQueryOptions<any>['onSet']>>(
        (_, query) => {
            removeParam();
            return query;
        },
        [removeParam]
    );

    const [ordering, setOrdering] = useSearchFilter<keyof OrderingsType>(
        'order',
        { method: 'replace' }
    );

    const [query, setQuery] = useSearchFilter<string>('query', {
        onSet,
        method: 'replace',
    });
    const [minPrice, setMinPrice] = useSearchFilter<number>('minPrice', {
        onSet,
        method: 'replace',
    });
    const [maxPrice, setMaxPrice] = useSearchFilter<number>('maxPrice', {
        onSet,
        method: 'replace',
    });

    return {
        query,
        minPrice,
        maxPrice,
        ordering: ordering ?? 'Newest',
        setQuery,
        setMinPrice,
        setMaxPrice,
        setOrdering,
    };
};

import React, { useCallback, useState } from 'react';
import {
    DropzoneOptions,
    ErrorCode,
    FileError,
    FileRejection,
} from 'react-dropzone';
import { AlertTitle } from '@mui/material';
import { useUploadPhotoMutation } from 'gql/generated/react';

export type UploadingFile = {
    resourceUrl?: string;
    file: File;
};

export const useDropzoneFiles = () => {
    const [files, setFiles] = useState<UploadingFile[]>([]);
    const [uploadPhoto] = useUploadPhotoMutation();

    const onDrop = useCallback<NonNullable<DropzoneOptions['onDrop']>>(
        async (acceptedFiles) => {
            try {
                for (const file of acceptedFiles) {
                    const { data } = await uploadPhoto({
                        variables: {
                            // TODO: Offload to webworker, or even better, just use a multipart upload
                            b64File: Buffer.from(
                                await file.arrayBuffer()
                            ).toString('base64'),
                            filename: file.name,
                            mimeType: file.type,
                        },
                    });

                    const resourceUrl = data?.uploadPublicFile?.resourceUrl;
                    setFiles((files) => [...files, { file, resourceUrl }]);
                }
            } catch (err) {
                console.error(err);
            }
        },
        [uploadPhoto]
    );

    const [errors, setErrors] = useState<JSX.Element[] | null>(null);

    const onDropRejected = useCallback<
        NonNullable<DropzoneOptions['onDropRejected']>
    >(async (rejectedFiles) => {
        setErrors(alertTitlesFrom(rejectedFiles));
    }, []);

    const deleteFiles = () => {
        setFiles([]);
    };
    const deleteFile = (toBeDeleted: UploadingFile) => {
        setFiles((files) => files.filter((f) => f !== toBeDeleted));
    };
    const clearErrors = () => setErrors(null);

    return {
        files,
        errors,
        onDrop,
        onDropRejected,
        clearErrors,
        deleteFile,
        deleteFiles,
    };
};

const fileErrorToString = (e: FileError) => {
    switch (e.code) {
        case ErrorCode.FileInvalidType:
            return 'File has an invalid type';
        case ErrorCode.FileTooLarge:
            return 'File is larger than 2 MB';
    }
};

const alertTitlesFrom = (rejectedFiles: FileRejection[]): JSX.Element[] => {
    return [
        <AlertTitle>The following files were rejected:</AlertTitle>,
        ...rejectedFiles.map((f) => {
            return (
                <AlertTitle>
                    · {f.file.name} -{' '}
                    {f.errors
                        .map((e) => {
                            return fileErrorToString(e);
                        })
                        .join(', ')}
                </AlertTitle>
            );
        }),
    ];
};

export const user_pages: Record<string, string> = {
    Profile: '/user/${userid}',
    'Create Listing': '/listing/create',
    Logout: '/logout',
};

const withPlugins = require('next-compose-plugins');
const withTM = require('next-transpile-modules')(['ui', 'gql', 'theme']);

/** @type {import('next').NextConfig} */
const config = {
    swcMinify: true,
    eslint: {
        ignoreDuringBuilds: true,
    },
    experimental: {
        concurrentFeatures: true,
    },

    // finally
    compiler: {
        emotion: true,
    },

    images: {
        domains: [
            's3.amazonaws.com',
            'placekitten.com',
            'static.wikia.nocookie.net',
            'i.pinimg.com',
            'loremflickr.com',
            'localhost',
        ], // TODO: remove
    },
};

module.exports = withPlugins([[withTM, { reactStrictMode: true }]], config);

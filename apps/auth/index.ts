import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import supertokens from 'supertokens-node';
import { middleware, errorHandler } from 'supertokens-node/framework/express';
import dotenv from 'dotenv';
import { getRecipeList } from './recipeList';

dotenv.config();

const port = process.env.PORT || 3535;
const apiDomain = process.env.SERVICE_URL!;
const websiteDomain = process.env.REDIRECT_URL!;
const supertokensUrl = process.env.SUPERTOKENS_URL!;

supertokens.init({
    framework: 'express',
    supertokens: {
        connectionURI: supertokensUrl,
    },
    appInfo: {
        appName: 'dumpster_fire',
        apiDomain,
        websiteDomain,
    },
    recipeList: getRecipeList(),
});

const app = express();

app.use(
    cors({
        origin: websiteDomain,
        allowedHeaders: ['content-type', ...supertokens.getAllCORSHeaders()],
        methods: ['GET', 'PUT', 'POST', 'DELETE'],
        credentials: true,
    })
);
app.use(helmet({ contentSecurityPolicy: false }));
app.use(middleware());
app.use(errorHandler());
app.use(
    (
        err: any,
        req: express.Request<any>,
        res: express.Response<any>,
        next: express.NextFunction
    ) => {
        res.status(500).send('Internal error: ' + err.message);
    }
);

app.listen(port, () => console.log(`Auth service listening on port ${port}`));

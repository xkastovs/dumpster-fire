import { GraphQLClient } from 'graphql-request';

export const getClient = () =>
    new GraphQLClient(process.env.GRAPHQL_URL ?? '', {
        headers: {
            'x-hasura-admin-secret': process.env.GRAPHQL_ADMIN_SECRET!,
        },
    });

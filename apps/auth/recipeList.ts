import Session from 'supertokens-node/recipe/session';
import ThirdPartyEmailPassword from 'supertokens-node/recipe/thirdpartyemailpassword';
import { getSdk } from 'gql/generated/server';
import { getClient } from './client';

export const getRecipeList = () => {
    const sdk = getSdk(getClient());

    return [
        ThirdPartyEmailPassword.init({
            signUpFeature: {
                formFields: [
                    {
                        id: 'name',
                        optional: false,
                        validate: async (value) => {
                            if (value == null) {
                                return 'Name must not be undefined!';
                            }
                            if (`${value}`.length == 0) {
                                return 'Name must not be empty!';
                            }

                            return undefined;
                        },
                    },
                ],
            },
            providers: [
                ThirdPartyEmailPassword.Google({
                    clientId: process.env.GOOGLE_CLIENT_ID!,
                    clientSecret: process.env.GOOGLE_CLIENT_SECRET!,
                }),
                ThirdPartyEmailPassword.Github({
                    clientId: process.env.GITHUB_CLIENT_ID!,
                    clientSecret: process.env.GITHUB_CLIENT_SECRET!,
                }),
                ThirdPartyEmailPassword.Apple({
                    clientId: process.env.APPLE_CLIENT_ID!,
                    clientSecret: {
                        keyId: process.env.APPLE_KEY_ID!,
                        teamId: process.env.APPLE_TEAM_ID!,
                        privateKey: process.env.APPLE_PRIVATE_KEY!,
                    },
                }),
            ],
            override: {
                apis(originalImplementation) {
                    return {
                        ...originalImplementation,

                        // override the email password sign up API
                        async emailPasswordSignUpPOST(input) {
                            if (!originalImplementation.emailPasswordSignUpPOST)
                                throw Error('Should never come here');

                            const response =
                                await originalImplementation.emailPasswordSignUpPOST(
                                    input
                                );

                            if (response.status === 'OK') {
                                const { id, email } = response.user;
                                const name = input.formFields.find(
                                    (f) => f.id == 'name'
                                )?.value;

                                try {
                                    await sdk.createUser({
                                        input: { id, email, name },
                                    });
                                    console.log(`Created user ${id}`);
                                } catch (err) {
                                    console.error(err);
                                    return {
                                        status: 'EMAIL_ALREADY_EXISTS_ERROR',
                                    };
                                }
                            }

                            return response;
                        },

                        // override the thirdparty sign in / up API
                        async thirdPartySignInUpPOST(input) {
                            if (!originalImplementation.thirdPartySignInUpPOST)
                                throw Error('Should never come here');

                            const response =
                                await originalImplementation.thirdPartySignInUpPOST(
                                    input
                                );

                            if (response.status === 'OK') {
                                if (response.createdNewUser) {
                                    if (response.status === 'OK') {
                                        const { id, email } = response.user;

                                        try {
                                            await sdk.createUser({
                                                input: { id, email },
                                            });
                                        } catch (err) {
                                            console.error(err);
                                            return {
                                                status: 'NO_EMAIL_GIVEN_BY_PROVIDER',
                                            };
                                        }
                                    }
                                }
                            }

                            return response;
                        },
                    };
                },
            },
        }),
        Session.init({
            jwt: {
                enable: true,
                issuer: process.env.JWT_ISSUER_URL,
            },
            override: {
                functions: (oI) => {
                    return {
                        ...oI,
                        createNewSession: async function (input) {
                            let userInfo =
                                await ThirdPartyEmailPassword.getUserById(
                                    input.userId
                                );

                            input.accessTokenPayload = {
                                ...input.accessTokenPayload,
                                'https://hasura.io/jwt/claims': {
                                    'x-hasura-user-id': userInfo?.id,
                                    'x-hasura-default-role': 'user',
                                    'x-hasura-allowed-roles': ['user'],
                                },
                            };

                            return oI.createNewSession(input);
                        },
                    };
                },
            },
        }),
    ];
};

alter table "app"."listings"
  add constraint "listings_thumbnail_id_fkey"
  foreign key ("thumbnail_id")
  references "app"."photos"
  ("id") on update restrict on delete restrict;

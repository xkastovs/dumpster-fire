CREATE TABLE "app"."users" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "name" varchar NOT NULL, "email" varchar, PRIMARY KEY ("id") , UNIQUE ("id"), UNIQUE ("name"), UNIQUE ("email"));COMMENT ON TABLE "app"."users" IS E'Global app users';
CREATE EXTENSION IF NOT EXISTS pgcrypto;

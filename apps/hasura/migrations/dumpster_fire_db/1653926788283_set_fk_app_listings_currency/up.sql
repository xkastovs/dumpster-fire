alter table "app"."listings"
  add constraint "listings_currency_fkey"
  foreign key ("currency")
  references "enums"."currency"
  ("value") on update restrict on delete restrict;

CREATE TABLE "app"."photos" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "listing_id" uuid NOT NULL, "link" text NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("listing_id") REFERENCES "app"."listings"("id") ON UPDATE restrict ON DELETE restrict);COMMENT ON TABLE "app"."photos" IS E'1-to-many in relation to listings';
CREATE EXTENSION IF NOT EXISTS pgcrypto;

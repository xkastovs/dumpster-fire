import dotenv from 'dotenv';
import { getClient } from './client';
import {
    CreateListingMutation,
    Enums_Currency_Enum,
    getSdk,
} from 'gql/generated/server';
import { faker } from '@faker-js/faker';
import { randomUUID } from 'crypto';

// AMOUNTS
const userAmount = 10;
const listingsPerUser = 10;
const photosPerListing = 10;

dotenv.config();

const client = getClient();
const sdk = getSdk(client);

function createUser() {
    const userId = randomUUID();
    const name = faker.name.findName();
    const email = faker.internet.email();

    return sdk.createUser({ input: { id: userId, email, name } });
}

async function createListing(userId: string) {
    const listingId = randomUUID();
    const listingTitle = faker.commerce.productName();
    const listingDescription = faker.commerce.productDescription();
    const listingPrice = +faker.commerce.price(50, 1000, 2);

    const listing = sdk.createListing({
        input: {
            id: listingId,
            title: listingTitle,
            description: listingDescription,
            price: listingPrice,
            creator_id: userId,
            currency: Enums_Currency_Enum.Czk,
        },
    });

    const thumbnailId = (await createPhoto(listing)).insert_app_photos_one?.id;

    // set the listings thumbnail
    sdk.editListing({
        id: { id: listingId },
        input: {
            id: listingId,
            title: listingTitle,
            description: listingDescription,
            price: listingPrice,
            creator_id: userId,
            currency: Enums_Currency_Enum.Czk,
            thumbnail_id: thumbnailId,
        },
    });

    // create additional listing photos
    for (let i = 0; i < photosPerListing - 1; ++i) {
        createPhoto(listing);
    }
}

async function createPhoto(listing: Promise<CreateListingMutation>) {
    const photoId = randomUUID();
    const photoLink = faker.image.cats(500, 500, true);
    const listingId = (await listing).insert_app_listings_one?.id;

    return sdk.addPhoto({
        photo: {
            id: photoId,
            link: photoLink,
            listing_id: listingId,
        },
    });
}

async function main() {
    for (let i = 0; i < userAmount; ++i) {
        try {
            const user = createUser();

            console.log(
                `created cat lover: ${(await user).insert_app_users_one?.name}`
            );

            for (let i = 0; i < listingsPerUser; ++i) {
                createListing((await user).insert_app_users_one?.id!);
            }
        } catch (error) {}
    }
}

main();

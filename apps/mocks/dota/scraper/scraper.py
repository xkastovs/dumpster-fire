import requests
from bs4 import BeautifulSoup
import uuid

baseURL = "https://dota2.fandom.com"
URL = f"{baseURL}/wiki/Items"
page = requests.get(URL)

soup = BeautifulSoup(page.content, "html.parser")
itemlists = soup.find(id="mw-content-text").find("div", class_="mw-parser-output").find_all("div", class_="itemlist")

output_file = open("data.ts", "w")

print('export const data = [', file=output_file)

for i, itemlist in enumerate(itemlists[:11]):
    for item in itemlist.find_all("div"):
        soup = BeautifulSoup(requests.get(baseURL + item.find("a")["href"]).content, "html.parser")
        table = soup.find("table", class_="infobox").find("tbody")
        rows = table.find_all("tr")[:6]
            
        listing_title = rows[0].find_all('div')[2].text.strip()
        photo_link = rows[1].find('img').get("data-src")
        description = rows[2].find('td').text.strip()
        price = table.find("table").find_all("tr")[1].find("b").text.strip()
        listing_id = str(uuid.uuid4())
        photos_id = str(uuid.uuid4())

        listing = {
            "listingId": listing_id,
            "title": listing_title,
            "description": description,
            "price": price,
            "photoId": photos_id,
            "link": photo_link
            }
        
        print("{", file=output_file)
        for key, value in listing.items():
            print(f'{key}: "{value}",', file=output_file)
        print("},", file=output_file)

print('];', file=output_file)

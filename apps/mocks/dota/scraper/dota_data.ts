export const data = [
    {
        listingId: '11c386d1-0fa6-4d10-860d-4944089256a3',
        title: 'Observer Ward',
        description:
            'A form of half-sentient plant, often cultivated by apprentice wizards.',
        price: '0',
        photoId: 'a6f59f66-d468-4c0e-ab51-5262cb36463a',
        link: 'https://static.wikia.nocookie.net/dota2_gamepedia/images/f/f6/Observer_Ward_icon.png/revision/latest/scale-to-width-down/100?cb=20160530171901',
    },
    {
        listingId: '6da6e85f-c4cc-4d98-a4e5-d95a15b14375',
        title: 'Clarity',
        description: 'Clear water that enhances the ability to meditate.',
        price: '50',
        photoId: 'a53fff81-e1dd-4637-9cd0-38f4bb7de4dd',
        link: 'https://static.wikia.nocookie.net/dota2_gamepedia/images/7/77/Clarity_icon.png/revision/latest/scale-to-width-down/100?cb=20160530165109',
    },
    {
        listingId: '15b7b375-855f-4e46-b09e-49e152b72840',
        title: 'Sentry Ward',
        description:
            'A form of plant originally grown in the garden of a fearful king.',
        price: '50',
        photoId: 'b081c436-2365-4bd5-bd5b-93cfb0b77632',
        link: 'https://static.wikia.nocookie.net/dota2_gamepedia/images/3/3e/Sentry_Ward_icon.png/revision/latest/scale-to-width-down/100?cb=20160530173231',
    },
    {
        listingId: '3cf4b99f-7308-4e59-bc8b-89b13128056c',
        title: 'Smoke of Deceit',
        description:
            "The charlatan wizard Myrddin's only true contribution to the arcane arts.",
        price: '50',
        photoId: 'be598940-17a3-4c02-aed1-af351e03b732',
        link: 'https://static.wikia.nocookie.net/dota2_gamepedia/images/0/04/Smoke_of_Deceit_icon.png/revision/latest/scale-to-width-down/100?cb=20160530173506',
    },
    {
        listingId: 'cbdc3c5f-4502-417a-a4b8-241ab038cee7',
        title: 'Enchanted Mango',
        description:
            'The bittersweet flavors of Jidi Isle are irresistible to amphibians.',
        price: '65',
        photoId: 'e82c7b9f-170f-4ca2-a3f9-980c1be63797',
        link: 'https://static.wikia.nocookie.net/dota2_gamepedia/images/7/70/Enchanted_Mango_icon.png/revision/latest/scale-to-width-down/100?cb=20160530170213',
    },
    {
        listingId: '4e2a99c8-1369-4660-a8c8-603a90050bde',
        title: 'Faerie Fire',
        description:
            'The ethereal flames from the ever-burning ruins of Kindertree ignite across realities.',
        price: '70',
        photoId: '6765524a-35f3-45de-a7a1-8dffc6918967',
        link: 'https://static.wikia.nocookie.net/dota2_gamepedia/images/5/54/Faerie_Fire_icon.png/revision/latest/scale-to-width-down/100?cb=20160530170344',
    },
];

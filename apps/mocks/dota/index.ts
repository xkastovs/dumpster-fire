import dotenv from 'dotenv';
import { getClient } from '../client';
import { Enums_Currency_Enum, getSdk } from 'gql/generated/server';
import { randomUUID } from 'crypto';
import { data } from './scraper/dota_data';
import { faker } from '@faker-js/faker';

// !DOESN'T WORK

dotenv.config();

const client = getClient();
const sdk = getSdk(client);

function createUser() {
    const userId = randomUUID();
    const name = faker.name.findName();
    const email = faker.internet.email();

    return sdk.createUser({ input: { id: userId, email, name } });
}

async function main() {
    for (const entry of data) {
        console.log(`creating title: ${entry.title}`);

        const userId = (await createUser()).insert_app_users_one?.id;
        const listingId = randomUUID();
        const photoId = randomUUID();

        const listing = sdk.createListing({
            input: {
                id: listingId,
                title: entry.title,
                description: entry.description,
                price: +entry.price,
                creator_id: userId,
                currency: Enums_Currency_Enum.Czk,
            },
        });

        sdk.addPhoto({
            photo: {
                id: photoId,
                link: entry.link,
                listing_id: listingId,
            },
        });

        sdk.editListing({
            id: { id: listingId },
            input: {
                id: listingId,
                title: entry.title,
                description: entry.description,
                price: +entry.price,
                creator_id: userId,
                currency: Enums_Currency_Enum.Czk,
                thumbnail_id: photoId,
            },
        });
    }
}

main();

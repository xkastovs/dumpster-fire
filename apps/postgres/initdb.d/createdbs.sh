#!/bin/bash
set -eu

function createDbAndUser() {
    local dbName=$1
    local dbUser=$2
    local dbPass=$3

    echo "Creating database $dbName with user $dbUser and password $dbPass"

    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
        CREATE DATABASE $dbName;
        CREATE USER $dbUser WITH PASSWORD '$dbPass';
        GRANT ALL PRIVILEGES ON DATABASE $dbName TO $dbUser;
EOSQL
}

for dbname in $(echo $DATABASES | tr "," " "); do

  createDbAndUser "$dbname" "$dbname" "${dbname}password"
done 
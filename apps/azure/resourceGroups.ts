import * as resources from '@pulumi/azure-native/resources';
import { config } from './config';

const resourceGroupName =
    config.get('azure-native:resourceGroupName') || 'dumpster-fire-rg';
const location = config.get('location') || 'germanywestcentral';

export const aksResourceGroup = new resources.ResourceGroup(resourceGroupName, {
    location,
    resourceGroupName,
});

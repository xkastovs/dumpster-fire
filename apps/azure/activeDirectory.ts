import * as azuread from '@pulumi/azuread';

export const adApp = new azuread.Application('aks', {
    displayName: 'aks',
});

export const adSp = new azuread.ServicePrincipal('aksSp', {
    applicationId: adApp.applicationId,
});

export const adSpPassword = new azuread.ServicePrincipalPassword(
    'aksSpPassword',
    {
        servicePrincipalId: adSp.id,
        endDate: '2099-01-01T00:00:00Z',
    }
);

import * as tls from '@pulumi/tls';
import * as containerservice from '@pulumi/azure-native/containerservice';
import { adApp, adSpPassword } from './activeDirectory';
import { aksResourceGroup } from './resourceGroups';
import { config } from './config';

export const managedClusterName =
    config.get('managedClusterName') || 'dumpster-fire-aks';
export const nodes = config.getNumber('nodes') || 2;

const sshKey = new tls.PrivateKey('ssh-key', {
    algorithm: 'RSA',
    rsaBits: 4096,
});

const cluster = new containerservice.ManagedCluster(managedClusterName, {
    resourceGroupName: aksResourceGroup.name,
    agentPoolProfiles: [
        {
            count: nodes,
            maxPods: 20,
            mode: 'System',
            name: 'agentpool',
            nodeLabels: {},
            osDiskSizeGB: 0,
            osType: 'Linux',
            type: 'VirtualMachineScaleSets',
            vmSize: 'standard_f2s_v2',
        },
    ],
    dnsPrefix: aksResourceGroup.name,
    enableRBAC: true,
    // kubernetesVersion: ''
    linuxProfile: {
        adminUsername: 'azure',
        ssh: {
            publicKeys: [
                {
                    keyData: sshKey.publicKeyOpenssh,
                },
            ],
        },
    },
    servicePrincipalProfile: {
        clientId: adApp.applicationId,
        secret: adSpPassword.value,
    },
});

const creds = containerservice.listManagedClusterUserCredentialsOutput({
    resourceGroupName: aksResourceGroup.name,
    resourceName: cluster.name,
});

export const encoded = creds.kubeconfigs[0].value;

import * as k8s from '@pulumi/kubernetes';
import { ns as development } from '../ns';

const label = 'pg';
const pvc = new k8s.core.v1.PersistentVolumeClaim('pg-pvc', {
    metadata: {
        namespace: development.id,
    },

    spec: {
        accessModes: ['ReadWriteOnce'],
        storageClassName: 'managed-premium',
        resources: {
            requests: {
                storage: '2Gi',
            },
        },
    },
});

// Lidl database lol
export const dbSeployment = new k8s.apps.v1.Deployment('pg-deployment', {
    metadata: {
        labels: {
            app: label,
        },
        namespace: development.id,
    },
    spec: {
        replicas: 1,
        selector: {
            matchLabels: {
                app: label,
            },
        },
        template: {
            metadata: {
                labels: {
                    app: label,
                },
            },
            spec: {
                volumes: [
                    {
                        name: 'data',
                        persistentVolumeClaim: {
                            claimName: pvc.id.apply((t) => t.split('/').pop()!),
                        },
                    },
                ],
                containers: [
                    {
                        name: 'pg-container',
                        image: 'dumpsterfireregistry.azurecr.io/dumpster-fire_postgres:latest',
                        imagePullPolicy: 'Always',
                        volumeMounts: [
                            {
                                name: 'data',
                                mountPath: '/var/lib/postgresql',
                            },
                        ],
                        env: [
                            { name: 'POSTGRES_USER', value: 'postgres' },
                            {
                                name: 'POSTGRES_PASSWORD',
                                value: 'postgrespassword',
                            },
                            { name: 'DATABASES', value: 'hasura,supertokens' },
                        ],
                        resources: {
                            requests: {
                                cpu: '200m',
                                memory: '200M',
                            },
                            limits: {
                                cpu: '400m',
                                memory: '400M',
                            },
                        },
                    },
                ],
            },
        },
    },
});

export const dbService = new k8s.core.v1.Service('pg-service', {
    metadata: {
        namespace: development.id,
    },
    spec: {
        type: 'ClusterIP',
        ports: [
            {
                port: 5432,
                targetPort: 5432,
            },
        ],
        selector: {
            app: label,
        },
    },
});

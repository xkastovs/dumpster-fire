import * as k8s from '@pulumi/kubernetes';
import { ns as development } from '../ns';
import { dbService } from '../pg';

const label = 'supertokens';

export const supertokensDeployment = new k8s.apps.v1.Deployment(
    'supertokens-deployment',
    {
        metadata: {
            labels: {
                app: label,
            },
            namespace: development.id,
        },
        spec: {
            replicas: 1,
            selector: {
                matchLabels: {
                    app: label,
                },
            },
            template: {
                metadata: {
                    labels: {
                        app: label,
                    },
                },
                spec: {
                    containers: [
                        {
                            name: 'supertokens-container',
                            image: 'dumpsterfireregistry.azurecr.io/dumpster-fire_supertokens:latest',
                            imagePullPolicy: 'Always',
                            env: [
                                {
                                    name: 'POSTGRESQL_CONNECTION_URI',
                                    value: dbService.id.apply(
                                        (v) =>
                                            `postgresql://postgres:postgrespassword@${v
                                                .split('/')
                                                .pop()}:5432/supertokens`
                                    ),
                                },
                                {
                                    name: 'POSTGRESQL_TABLE_SCHEMA',
                                    value: 'users',
                                },
                            ],
                            resources: {
                                requests: {
                                    cpu: '200m',
                                    memory: '200M',
                                },
                                limits: {
                                    cpu: '400m',
                                    memory: '400M',
                                },
                            },
                        },
                    ],
                },
            },
        },
    }
);

export const supertokensService = new k8s.core.v1.Service(
    'supertokens-service',
    {
        metadata: {
            namespace: development.id,
        },
        spec: {
            type: 'ClusterIP',
            ports: [
                {
                    port: 3567,
                    targetPort: 3567,
                },
            ],
            selector: {
                app: label,
            },
        },
    }
);

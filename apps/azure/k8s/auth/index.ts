import * as k8s from '@pulumi/kubernetes';
import { hasuraService } from '../hasura/hasuraService';
import { hasuraSecret } from '../hasura/config';
import { ns as development } from '../ns';
import { supertokensService } from '../supertokens';

const label = 'auth';

export const authDeployment = new k8s.apps.v1.Deployment('auth-deployment', {
    metadata: {
        labels: {
            app: label,
        },
        namespace: development.id,
    },
    spec: {
        replicas: 1,
        selector: {
            matchLabels: {
                app: label,
            },
        },
        template: {
            metadata: {
                labels: {
                    app: label,
                },
            },
            spec: {
                containers: [
                    {
                        name: 'auth-container',
                        image: 'dumpsterfireregistry.azurecr.io/dumpster-fire_auth:latest',
                        imagePullPolicy: 'Always',
                        // These are testing keys, i don't care about them
                        env: [
                            {
                                name: 'GOOGLE_CLIENT_ID',
                                value: '1060725074195-kmeum4crr01uirfl2op9kd5acmi9jutn.apps.googleusercontent.com',
                            },
                            {
                                name: 'GOOGLE_CLIENT_SECRET',
                                value: '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',
                            },
                            {
                                name: 'APPLE_PRIVATE_KEY',
                                value: '-----BEGIN PRIVATE KEY-----\nMIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgu8gXs+XYkqXD6Ala9Sf/iJXzhbwcoG5dMh1OonpdJUmgCgYIKoZIzj0DAQehRANCAASfrvlFbFCYqn3I2zeknYXLwtH30JuOKestDbSfZYxZNMqhF/OzdZFTV0zc5u5s3eN+oCWbnvl0hM+9IW0UlkdA\n-----END PRIVATE KEY-----',
                            },
                            {
                                name: 'APPLE_CLIENT_ID',
                                value: '4398792-io.supertokens.example.service',
                            },
                            { name: 'APPLE_KEY_ID', value: '7M48Y4RYDL' },
                            { name: 'APPLE_TEAM_ID', value: 'YWQCXGJRJL' },
                            {
                                name: 'GITHUB_CLIENT_ID',
                                value: '467101b197249757c71f',
                            },
                            {
                                name: 'GITHUB_CLIENT_SECRET',
                                value: 'e97051221f4b6426e8fe8d51486396703012f5bd',
                            },
                            { name: 'PORT', value: '3535' },
                            {
                                name: 'SERVICE_URL',
                                value: 'http://localhost:3535',
                            },
                            {
                                name: 'REDIRECT_URL',
                                value: 'http://localhost:3000', //? TODO:
                            },
                            {
                                name: 'GRAPHQL_URL',
                                value: hasuraService.id.apply(
                                    (t) =>
                                        `http://${t
                                            .split('/')
                                            .pop()}:8080/v1/graphql`
                                ),
                            },
                            {
                                name: 'GRAPHQL_ADMIN_SECRET',
                                value: hasuraSecret,
                            },
                            {
                                name: 'SUPERTOKENS_URL',
                                value: supertokensService.id.apply(
                                    (t) => `http://${t.split('/').pop()}:3567`
                                ),
                            },
                        ],
                        resources: {
                            requests: {
                                cpu: '200m',
                                memory: '200M',
                            },
                            limits: {
                                cpu: '400m',
                                memory: '400M',
                            },
                        },
                    },
                ],
            },
        },
    },
});

export const authService = new k8s.core.v1.Service('auth-service', {
    metadata: {
        namespace: development.id,
    },
    spec: {
        type: 'ClusterIP',
        ports: [
            {
                port: 3535,
                targetPort: 3535,
            },
        ],
        selector: {
            app: label,
        },
    },
});

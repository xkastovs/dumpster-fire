import * as k8s from '@pulumi/kubernetes';
import { ns as development } from '../ns';
import { label } from './index';

export const filesService = new k8s.core.v1.Service('files-service', {
    metadata: {
        namespace: development.id,
    },
    spec: {
        type: 'ClusterIP',
        ports: [
            {
                port: 8084,
                targetPort: 8084,
            },
        ],
        selector: {
            app: label,
        },
    },
});

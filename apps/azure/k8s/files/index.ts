import * as k8s from '@pulumi/kubernetes';
import {
    minioHost,
    minioAccessKey,
    minioBucket,
    minioPort,
    minioSecretKey,
} from '../minio/config';
import { ns as development } from '../ns';

export const label = 'files';

export const filesDeployment = new k8s.apps.v1.Deployment('files-deployment', {
    metadata: {
        labels: {
            app: label,
        },
        namespace: development.id,
    },
    spec: {
        replicas: 1,
        selector: {
            matchLabels: {
                app: label,
            },
        },
        template: {
            metadata: {
                labels: {
                    app: label,
                },
            },
            spec: {
                containers: [
                    {
                        name: 'files-container',
                        image: 'dumpsterfireregistry.azurecr.io/dumpster-fire_files:latest',
                        imagePullPolicy: 'Always',
                        env: [
                            {
                                name: 'MINIO_ACCESS_KEY',
                                value: minioAccessKey,
                            },
                            {
                                name: 'MINIO_SECRET_KEY',
                                value: minioSecretKey,
                            },
                            {
                                name: 'MINIO_ENDPOINT',
                                value: minioHost,
                            },
                            {
                                name: 'MINIO_PORT',
                                value: `${minioPort}`,
                            },
                            {
                                name: 'BUCKET_NAME',
                                value: minioBucket,
                            },
                        ],
                        resources: {
                            requests: {
                                cpu: '200m',
                                memory: '200M',
                            },
                            limits: {
                                cpu: '400m',
                                memory: '400M',
                            },
                        },
                    },
                ],
            },
        },
    },
});

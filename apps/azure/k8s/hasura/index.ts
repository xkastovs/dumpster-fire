import * as k8s from '@pulumi/kubernetes';
import { authService } from '../auth';
import { filesService } from '../files/filesService';
import { ns as development } from '../ns';
import { dbService } from '../pg';
import { hasuraSecret } from './config';

export const label = 'hasura';

export const hasuraDeployment = new k8s.apps.v1.Deployment(
    'hasura-deployment',
    {
        metadata: {
            labels: {
                app: label,
            },
            namespace: development.id,
        },
        spec: {
            replicas: 1,
            selector: {
                matchLabels: {
                    app: label,
                },
            },
            template: {
                metadata: {
                    labels: {
                        app: label,
                    },
                },
                spec: {
                    containers: [
                        {
                            name: 'hasura-container',
                            image: 'dumpsterfireregistry.azurecr.io/dumpster-fire_graphql-engine:latest',
                            imagePullPolicy: 'Always',
                            env: [
                                {
                                    name: 'HASURA_GRAPHQL_METADATA_DATABASE_URL',
                                    value: dbService.id.apply(
                                        (v) =>
                                            `postgres://postgres:postgrespassword@${v
                                                .split('/')
                                                .pop()}:5432/hasura`
                                    ),
                                },
                                {
                                    name: 'PG_DATABASE_URL',
                                    value: dbService.id.apply(
                                        (v) =>
                                            `postgres://postgres:postgrespassword@${v
                                                .split('/')
                                                .pop()}:5432/hasura`
                                    ),
                                },
                                {
                                    name: 'HASURA_GRAPHQL_ENABLE_CONSOLE',
                                    value: 'true',
                                },
                                {
                                    name: 'HASURA_GRAPHQL_ENABLED_LOG_TYPES',
                                    value: 'startup, http-log, webhook-log, websocket-log, query-log',
                                },
                                {
                                    name: 'HASURA_GRAPHQL_MIGRATIONS_DIR',
                                    value: '/hasura-migrations',
                                },
                                {
                                    name: 'HASURA_GRAPHQL_METADATA_DIR',
                                    value: '/hasura-metadata',
                                },
                                {
                                    name: 'HASURA_GRAPHQL_ACTIONS_HANDLER_WEBHOOK_BASEURL',
                                    value: filesService.id.apply(
                                        (t) =>
                                            `http://${t.split('/').pop()}:8084`
                                    ),
                                },
                                {
                                    name: 'HASURA_GRAPHQL_JWT_SECRET',
                                    value: authService.id.apply(
                                        (t) =>
                                            `{ "jwk_url": "http://${t
                                                .split('/')
                                                .pop()}:3535/auth/jwt/jwks.json" }`
                                    ),
                                },
                                {
                                    // TODO: pulumi secret
                                    name: 'HASURA_GRAPHQL_ADMIN_SECRET',
                                    value: hasuraSecret,
                                },
                                {
                                    name: 'HASURA_GRAPHQL_UNAUTHORIZED_ROLE',
                                    value: 'other',
                                },
                            ],
                            startupProbe: {
                                httpGet: {
                                    path: '/',
                                    port: 8080,
                                },
                                initialDelaySeconds: 1,
                                failureThreshold: 12,
                                periodSeconds: 5,
                                timeoutSeconds: 1,
                            },
                            readinessProbe: {
                                httpGet: {
                                    path: '/',
                                    port: 8080,
                                },
                                initialDelaySeconds: 5,
                                periodSeconds: 5,
                                timeoutSeconds: 2,
                            },
                            livenessProbe: {
                                httpGet: {
                                    path: '/',
                                    port: 8080,
                                },
                                initialDelaySeconds: 5,
                                periodSeconds: 5,
                                timeoutSeconds: 2,
                            },
                            resources: {
                                requests: {
                                    cpu: '200m',
                                    memory: '200M',
                                },
                                limits: {
                                    cpu: '400m',
                                    memory: '400M',
                                },
                            },
                        },
                    ],
                },
            },
        },
    }
);

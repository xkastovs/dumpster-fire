import * as k8s from '@pulumi/kubernetes';
import { ns as development } from '../ns';
import { label } from './index';

export const hasuraService = new k8s.core.v1.Service('hasura-service', {
    metadata: {
        namespace: development.id,
    },
    spec: {
        type: 'ClusterIP',
        ports: [
            {
                port: 8080,
                targetPort: 8080,
            },
        ],
        selector: {
            app: label,
        },
    },
});

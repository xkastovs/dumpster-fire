import * as k8s from '@pulumi/kubernetes';
import { ns } from './ns';

export const ingress = new k8s.networking.v1beta1.Ingress('ingress', {
    metadata: {
        namespace: ns.id,
        annotations: { 'kubernetes.io/ingress.class': 'nginx' },
    },
    spec: {
        rules: [
            {
                host: 'dumpster-fire.cz',
                http: {
                    paths: [
                        // {
                        //     path: '/',
                        //     backend: {
                        //         serviceName: 'frontend-service-f70298c9',
                        //         servicePort: 80,
                        //     },
                        // },
                        {
                            path: '/v1/graphql',
                            backend: {
                                serviceName: 'hasura-service-f70298c9',
                                servicePort: 8080,
                            },
                        },
                    ],
                },
            },
            {
                host: 'auth.dumpster-fire.cz',
                http: {
                    paths: [
                        {
                            path: '/',
                            backend: {
                                serviceName: 'auth-service-5b81540d',
                                servicePort: 3567,
                            },
                        },
                    ],
                },
            },
        ],
    },
});

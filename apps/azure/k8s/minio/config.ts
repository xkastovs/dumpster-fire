export const minioAccessKey = 'minioAccessKey';
export const minioSecretKey = 'minioSecretKey';
export const minioHost = 'minio';
export const minioPort = 9000;
export const minioBucket = 'content';

import * as k8s from '@pulumi/kubernetes';
import { ns as development } from '../ns';
import { minioHost, minioAccessKey, minioPort, minioSecretKey } from './config';

const label = 'minio';
const pvc = new k8s.core.v1.PersistentVolumeClaim('minio-pvc', {
    metadata: {
        namespace: development.id,
    },

    spec: {
        accessModes: ['ReadWriteOnce'],
        storageClassName: 'managed-premium',
        resources: {
            requests: {
                storage: '2Gi',
            },
        },
    },
});

export const minioDeployment = new k8s.apps.v1.Deployment('minio-deployment', {
    metadata: {
        labels: {
            app: label,
        },
        namespace: development.id,
    },
    spec: {
        replicas: 1,
        selector: {
            matchLabels: {
                app: label,
            },
        },
        template: {
            metadata: {
                labels: {
                    app: label,
                },
            },
            spec: {
                volumes: [
                    {
                        name: 'data',
                        persistentVolumeClaim: {
                            claimName: pvc.id.apply((t) => t.split('/').pop()!),
                        },
                    },
                ],
                containers: [
                    {
                        name: 'minio-container',
                        image: 'dumpsterfireregistry.azurecr.io/dumpster-fire_minio:latest',
                        imagePullPolicy: 'Always',
                        // TODO: pulumi secrets
                        env: [
                            {
                                name: 'MINIO_ACCESS_KEY',
                                value: minioAccessKey,
                            },
                            {
                                name: 'MINIO_SECRET_KEY',
                                value: minioSecretKey,
                            },
                            {
                                name: 'MINIO_HOST',
                                value: minioHost,
                            },
                            {
                                name: 'MINIO_PORT',
                                value: `${minioPort}`,
                            },
                        ],
                        volumeMounts: [
                            {
                                name: 'data',
                                mountPath: '/data',
                            },
                        ],
                        resources: {
                            requests: {
                                cpu: '200m',
                                memory: '200M',
                            },
                            limits: {
                                cpu: '400m',
                                memory: '400M',
                            },
                        },
                    },
                ],
            },
        },
    },
});

export const minioService = new k8s.core.v1.Service('minio-service', {
    metadata: {
        namespace: development.id,
    },
    spec: {
        type: 'ClusterIP',
        ports: [
            {
                name: 'minio-port',
                port: 9000,
                targetPort: 9000,
            },
            {
                name: 'console',
                port: 9001,
                targetPort: 9001,
            },
        ],
        selector: {
            app: label,
        },
    },
});

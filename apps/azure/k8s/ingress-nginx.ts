import * as nginx from '@pulumi/kubernetes-ingress-nginx';

export const ingressController = new nginx.IngressController('ingress', {
    controller: {
        publishService: {
            enabled: true,
        },
    },
});

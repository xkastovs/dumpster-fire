import * as azure_native from '@pulumi/azure-native';
import { config } from './config';
import { aksResourceGroup } from './resourceGroups';

const registryName = config.get('registryName') || 'dumpsterfireregistry';

export const registry = new azure_native.containerregistry.Registry(
    registryName,
    {
        adminUserEnabled: true,
        registryName,
        resourceGroupName: aksResourceGroup.name,
        sku: {
            name: 'Basic',
        },
    }
);

import { encoded } from './aks';
import { registry } from './registry';
import './k8s';

export const kubeconfig = encoded.apply((enc) =>
    Buffer.from(enc, 'base64').toString()
);

export const server = registry.loginServer;

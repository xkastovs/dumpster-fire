import { render } from '@testing-library/react';
import { PrimaryButton } from './Button';

describe('Button', () => {
    it('should render correctly', () => {
        const { container } = render(<PrimaryButton />);
        expect(container).toMatchSnapshot();
    });
});

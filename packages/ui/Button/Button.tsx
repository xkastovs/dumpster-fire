import { FC, PropsWithChildren } from 'react';
import { theme_dark } from 'theme/index';
import { Button, ButtonProps } from '@mui/material';

export const PrimaryButton: FC<PropsWithChildren<ButtonProps>> = (props) => {
    return <Button sx={ButtonStyle} {...props}></Button>;
};

const ButtonStyle = {
    alignItems: 'center',
    backgroundImage: theme_dark.colors.primary,
    border: '0',
    borderRadius: '8px',
    boxShadow: 'rgba(151, 65, 252, 0.2) 0 15px 30px -5px',
    boxSizing: 'border-box',
    color: '#ffff',
    display: 'flex',
    fontFamily: 'Phantomsans, sans-serif',
    fontSize: ' 20px',
    justifyContent: 'center',
    lineHeight: '1em',
    maxWidth: '100%',
    minWidth: '140px',
    padding: '19px 24px',
    textDecoration: 'none',
    userSelect: 'none',

    touchAction: 'manipulation',
    whiteSpace: 'nowrap',
    cursor: 'pointer',
    '&:hover': {
        outline: '0',
    },
};

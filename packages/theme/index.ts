export const theme_dark = {
    colors: {
        primary: 'linear-gradient(91.2deg, #00f1ff 0%, #ff019a 100%)',
        bacground: '#3A3A3A',
        secondary: '#EFEFEF',
        accent: '#00F1FF',
        accent_secondary: '#FF019A',
        font_light: '#FFFF',
        font_dark: '#3A3A3A',
    },
    shadows: {
        cardHover: '-1px 10px 29px 0px rgba(255,255,255,0.8)',
    },
    margins: {
        defaultCard: '2rem',
        defaultTop: '4rem',
    },
};

export const theme_light = {
    colors: {
        primary: '#ffff',
        primary_dark: 'primary-dark',
        secondary: 'secondary',
        accent: 'accent',
        font_color: 'font',
    },
};

import { GraphQLClient } from 'graphql-request';
import * as Dom from 'graphql-request/dist/types.dom';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
    [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
    [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
    [SubKey in K]: Maybe<T[SubKey]>;
};
export const Detailed_ListingFragmentDoc = gql`
    fragment detailed_listing on app_listings {
        id
        title
        description
        price
        currency
        created_at
        is_open
        thumbnail {
            id
            link
        }
        photos {
            id
            link
        }
        creator {
            id
            name
        }
    }
`;
export const Form_ListingFragmentDoc = gql`
    fragment form_listing on app_listings {
        id
        title
        description
        price
        currency
        is_selling
        photos {
            id
        }
        creator {
            id
        }
    }
`;
export const Photo_LinkFragmentDoc = gql`
    fragment photo_link on app_photos {
        link
    }
`;
export const List_ListingFragmentDoc = gql`
    fragment list_listing on app_listings {
        id
        title
        price
        currency
        thumbnail {
            id
            link
        }
        created_at
        creator {
            id
            name
        }
    }
`;
export const User_ProfileFragmentDoc = gql`
    fragment user_profile on app_users {
        id
        name
        email
        listings {
            ...list_listing
        }
    }
    ${List_ListingFragmentDoc}
`;
export const CreateListingDocument = gql`
    mutation createListing($input: app_listings_insert_input!) {
        insert_app_listings_one(object: $input) {
            ...detailed_listing
        }
    }
    ${Detailed_ListingFragmentDoc}
`;
export const DeleteListingDocument = gql`
    mutation deleteListing($id: uuid!) {
        delete_app_listings_by_pk(id: $id) {
            id
        }
    }
`;
export const EditListingDocument = gql`
    mutation editListing(
        $id: app_listings_pk_columns_input!
        $input: app_listings_set_input!
    ) {
        update_app_listings_by_pk(_set: $input, pk_columns: $id) {
            ...detailed_listing
        }
    }
    ${Detailed_ListingFragmentDoc}
`;
export const GetFormListingDocument = gql`
    query getFormListing($id: uuid!) {
        app_listings_by_pk(id: $id) {
            ...form_listing
        }
    }
    ${Form_ListingFragmentDoc}
`;
export const GetListingDocument = gql`
    query getListing($id: uuid!) {
        app_listings_by_pk(id: $id) {
            ...detailed_listing
        }
    }
    ${Detailed_ListingFragmentDoc}
`;
export const GetListingsDocument = gql`
    query getListings(
        $filter: app_listings_bool_exp
        $limit: Int!
        $offset: Int!
        $order_by: app_listings_order_by!
    ) {
        app_listings(
            where: $filter
            limit: $limit
            offset: $offset
            order_by: [$order_by]
        ) {
            ...list_listing
        }
        app_listings_aggregate(distinct_on: [id], where: $filter) {
            aggregate {
                count(columns: [id], distinct: true)
            }
        }
    }
    ${List_ListingFragmentDoc}
`;
export const UpsertFormListingDocument = gql`
    mutation upsertFormListing($input: app_listings_insert_input!) {
        insert_app_listings(
            objects: [$input]
            on_conflict: {
                constraint: listings_pkey
                update_columns: [title, description, price, currency, is_open]
            }
        ) {
            returning {
                ...form_listing
            }
        }
    }
    ${Form_ListingFragmentDoc}
`;
export const AddPhotoDocument = gql`
    mutation addPhoto($photo: app_photos_insert_input!) {
        insert_app_photos_one(object: $photo) {
            id
        }
    }
`;
export const GetPhotosDocument = gql`
    query getPhotos($listing_id: uuid!) {
        app_photos(where: { listing_id: { _eq: $listing_id } }) {
            ...photo_link
        }
    }
    ${Photo_LinkFragmentDoc}
`;
export const UploadPhotoDocument = gql`
    mutation uploadPhoto(
        $b64File: String!
        $filename: String!
        $mimeType: String!
    ) {
        uploadPublicFile(
            file: { content: $b64File, name: $filename, mimeType: $mimeType }
        ) {
            id
            name
            mimeType
            resourceUrl
        }
    }
`;
export const CreateUserDocument = gql`
    mutation createUser($input: app_users_insert_input!) {
        insert_app_users_one(object: $input) {
            ...user_profile
        }
    }
    ${User_ProfileFragmentDoc}
`;
export const GetUserProfileDocument = gql`
    query getUserProfile($id: uuid!) {
        app_users_by_pk(id: $id) {
            ...user_profile
        }
    }
    ${User_ProfileFragmentDoc}
`;
export const UpdateUserProfileDocument = gql`
    mutation updateUserProfile(
        $id: app_users_pk_columns_input!
        $input: app_users_set_input!
    ) {
        update_app_users_by_pk(_set: $input, pk_columns: $id) {
            ...user_profile
        }
    }
    ${User_ProfileFragmentDoc}
`;

export type SdkFunctionWrapper = <T>(
    action: (requestHeaders?: Record<string, string>) => Promise<T>,
    operationName: string,
    operationType?: string
) => Promise<T>;

const defaultWrapper: SdkFunctionWrapper = (
    action,
    _operationName,
    _operationType
) => action();

export function getSdk(
    client: GraphQLClient,
    withWrapper: SdkFunctionWrapper = defaultWrapper
) {
    return {
        createListing(
            variables: CreateListingMutationVariables,
            requestHeaders?: Dom.RequestInit['headers']
        ): Promise<CreateListingMutation> {
            return withWrapper(
                (wrappedRequestHeaders) =>
                    client.request<CreateListingMutation>(
                        CreateListingDocument,
                        variables,
                        { ...requestHeaders, ...wrappedRequestHeaders }
                    ),
                'createListing',
                'mutation'
            );
        },
        deleteListing(
            variables: DeleteListingMutationVariables,
            requestHeaders?: Dom.RequestInit['headers']
        ): Promise<DeleteListingMutation> {
            return withWrapper(
                (wrappedRequestHeaders) =>
                    client.request<DeleteListingMutation>(
                        DeleteListingDocument,
                        variables,
                        { ...requestHeaders, ...wrappedRequestHeaders }
                    ),
                'deleteListing',
                'mutation'
            );
        },
        editListing(
            variables: EditListingMutationVariables,
            requestHeaders?: Dom.RequestInit['headers']
        ): Promise<EditListingMutation> {
            return withWrapper(
                (wrappedRequestHeaders) =>
                    client.request<EditListingMutation>(
                        EditListingDocument,
                        variables,
                        { ...requestHeaders, ...wrappedRequestHeaders }
                    ),
                'editListing',
                'mutation'
            );
        },
        getFormListing(
            variables: GetFormListingQueryVariables,
            requestHeaders?: Dom.RequestInit['headers']
        ): Promise<GetFormListingQuery> {
            return withWrapper(
                (wrappedRequestHeaders) =>
                    client.request<GetFormListingQuery>(
                        GetFormListingDocument,
                        variables,
                        { ...requestHeaders, ...wrappedRequestHeaders }
                    ),
                'getFormListing',
                'query'
            );
        },
        getListing(
            variables: GetListingQueryVariables,
            requestHeaders?: Dom.RequestInit['headers']
        ): Promise<GetListingQuery> {
            return withWrapper(
                (wrappedRequestHeaders) =>
                    client.request<GetListingQuery>(
                        GetListingDocument,
                        variables,
                        { ...requestHeaders, ...wrappedRequestHeaders }
                    ),
                'getListing',
                'query'
            );
        },
        getListings(
            variables: GetListingsQueryVariables,
            requestHeaders?: Dom.RequestInit['headers']
        ): Promise<GetListingsQuery> {
            return withWrapper(
                (wrappedRequestHeaders) =>
                    client.request<GetListingsQuery>(
                        GetListingsDocument,
                        variables,
                        { ...requestHeaders, ...wrappedRequestHeaders }
                    ),
                'getListings',
                'query'
            );
        },
        upsertFormListing(
            variables: UpsertFormListingMutationVariables,
            requestHeaders?: Dom.RequestInit['headers']
        ): Promise<UpsertFormListingMutation> {
            return withWrapper(
                (wrappedRequestHeaders) =>
                    client.request<UpsertFormListingMutation>(
                        UpsertFormListingDocument,
                        variables,
                        { ...requestHeaders, ...wrappedRequestHeaders }
                    ),
                'upsertFormListing',
                'mutation'
            );
        },
        addPhoto(
            variables: AddPhotoMutationVariables,
            requestHeaders?: Dom.RequestInit['headers']
        ): Promise<AddPhotoMutation> {
            return withWrapper(
                (wrappedRequestHeaders) =>
                    client.request<AddPhotoMutation>(
                        AddPhotoDocument,
                        variables,
                        { ...requestHeaders, ...wrappedRequestHeaders }
                    ),
                'addPhoto',
                'mutation'
            );
        },
        getPhotos(
            variables: GetPhotosQueryVariables,
            requestHeaders?: Dom.RequestInit['headers']
        ): Promise<GetPhotosQuery> {
            return withWrapper(
                (wrappedRequestHeaders) =>
                    client.request<GetPhotosQuery>(
                        GetPhotosDocument,
                        variables,
                        { ...requestHeaders, ...wrappedRequestHeaders }
                    ),
                'getPhotos',
                'query'
            );
        },
        uploadPhoto(
            variables: UploadPhotoMutationVariables,
            requestHeaders?: Dom.RequestInit['headers']
        ): Promise<UploadPhotoMutation> {
            return withWrapper(
                (wrappedRequestHeaders) =>
                    client.request<UploadPhotoMutation>(
                        UploadPhotoDocument,
                        variables,
                        { ...requestHeaders, ...wrappedRequestHeaders }
                    ),
                'uploadPhoto',
                'mutation'
            );
        },
        createUser(
            variables: CreateUserMutationVariables,
            requestHeaders?: Dom.RequestInit['headers']
        ): Promise<CreateUserMutation> {
            return withWrapper(
                (wrappedRequestHeaders) =>
                    client.request<CreateUserMutation>(
                        CreateUserDocument,
                        variables,
                        { ...requestHeaders, ...wrappedRequestHeaders }
                    ),
                'createUser',
                'mutation'
            );
        },
        getUserProfile(
            variables: GetUserProfileQueryVariables,
            requestHeaders?: Dom.RequestInit['headers']
        ): Promise<GetUserProfileQuery> {
            return withWrapper(
                (wrappedRequestHeaders) =>
                    client.request<GetUserProfileQuery>(
                        GetUserProfileDocument,
                        variables,
                        { ...requestHeaders, ...wrappedRequestHeaders }
                    ),
                'getUserProfile',
                'query'
            );
        },
        updateUserProfile(
            variables: UpdateUserProfileMutationVariables,
            requestHeaders?: Dom.RequestInit['headers']
        ): Promise<UpdateUserProfileMutation> {
            return withWrapper(
                (wrappedRequestHeaders) =>
                    client.request<UpdateUserProfileMutation>(
                        UpdateUserProfileDocument,
                        variables,
                        { ...requestHeaders, ...wrappedRequestHeaders }
                    ),
                'updateUserProfile',
                'mutation'
            );
        },
    };
}
export type Sdk = ReturnType<typeof getSdk>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
    numeric: number;
    timestamptz: string;
    uuid: string;
};

export type Boolean_Cast_Exp = {
    String?: InputMaybe<String_Comparison_Exp>;
};

/** Boolean expression to compare columns of type "Boolean". All fields are combined with logical 'AND'. */
export type Boolean_Comparison_Exp = {
    _cast?: InputMaybe<Boolean_Cast_Exp>;
    _eq?: InputMaybe<Scalars['Boolean']>;
    _gt?: InputMaybe<Scalars['Boolean']>;
    _gte?: InputMaybe<Scalars['Boolean']>;
    _in?: InputMaybe<Array<Scalars['Boolean']>>;
    _is_null?: InputMaybe<Scalars['Boolean']>;
    _lt?: InputMaybe<Scalars['Boolean']>;
    _lte?: InputMaybe<Scalars['Boolean']>;
    _neq?: InputMaybe<Scalars['Boolean']>;
    _nin?: InputMaybe<Array<Scalars['Boolean']>>;
};

export type FileResource = {
    __typename?: 'FileResource';
    id: Scalars['String'];
    mimeType: Scalars['String'];
    name: Scalars['String'];
    resourceUrl: Scalars['String'];
};

/** Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
    _eq?: InputMaybe<Scalars['String']>;
    _gt?: InputMaybe<Scalars['String']>;
    _gte?: InputMaybe<Scalars['String']>;
    /** does the column match the given case-insensitive pattern */
    _ilike?: InputMaybe<Scalars['String']>;
    _in?: InputMaybe<Array<Scalars['String']>>;
    /** does the column match the given POSIX regular expression, case insensitive */
    _iregex?: InputMaybe<Scalars['String']>;
    _is_null?: InputMaybe<Scalars['Boolean']>;
    /** does the column match the given pattern */
    _like?: InputMaybe<Scalars['String']>;
    _lt?: InputMaybe<Scalars['String']>;
    _lte?: InputMaybe<Scalars['String']>;
    _neq?: InputMaybe<Scalars['String']>;
    /** does the column NOT match the given case-insensitive pattern */
    _nilike?: InputMaybe<Scalars['String']>;
    _nin?: InputMaybe<Array<Scalars['String']>>;
    /** does the column NOT match the given POSIX regular expression, case insensitive */
    _niregex?: InputMaybe<Scalars['String']>;
    /** does the column NOT match the given pattern */
    _nlike?: InputMaybe<Scalars['String']>;
    /** does the column NOT match the given POSIX regular expression, case sensitive */
    _nregex?: InputMaybe<Scalars['String']>;
    /** does the column NOT match the given SQL regular expression */
    _nsimilar?: InputMaybe<Scalars['String']>;
    /** does the column match the given POSIX regular expression, case sensitive */
    _regex?: InputMaybe<Scalars['String']>;
    /** does the column match the given SQL regular expression */
    _similar?: InputMaybe<Scalars['String']>;
};

export type UploadPublicFileInput = {
    content: Scalars['String'];
    mimeType: Scalars['String'];
    name: Scalars['String'];
};

/** Global application listings */
export type App_Listings = {
    __typename?: 'app_listings';
    created_at: Scalars['timestamptz'];
    /** An object relationship */
    creator: App_Users;
    creator_id: Scalars['uuid'];
    currency: Enums_Currency_Enum;
    description?: Maybe<Scalars['String']>;
    id: Scalars['uuid'];
    is_open: Scalars['Boolean'];
    is_selling: Scalars['Boolean'];
    /** An array relationship */
    photos: Array<App_Photos>;
    /** An aggregate relationship */
    photos_aggregate: App_Photos_Aggregate;
    price: Scalars['numeric'];
    /** An object relationship */
    thumbnail?: Maybe<App_Photos>;
    thumbnail_id?: Maybe<Scalars['uuid']>;
    title: Scalars['String'];
};

/** Global application listings */
export type App_ListingsPhotosArgs = {
    distinct_on?: InputMaybe<Array<App_Photos_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Photos_Order_By>>;
    where?: InputMaybe<App_Photos_Bool_Exp>;
};

/** Global application listings */
export type App_ListingsPhotos_AggregateArgs = {
    distinct_on?: InputMaybe<Array<App_Photos_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Photos_Order_By>>;
    where?: InputMaybe<App_Photos_Bool_Exp>;
};

/** aggregated selection of "app.listings" */
export type App_Listings_Aggregate = {
    __typename?: 'app_listings_aggregate';
    aggregate?: Maybe<App_Listings_Aggregate_Fields>;
    nodes: Array<App_Listings>;
};

/** aggregate fields of "app.listings" */
export type App_Listings_Aggregate_Fields = {
    __typename?: 'app_listings_aggregate_fields';
    avg?: Maybe<App_Listings_Avg_Fields>;
    count: Scalars['Int'];
    max?: Maybe<App_Listings_Max_Fields>;
    min?: Maybe<App_Listings_Min_Fields>;
    stddev?: Maybe<App_Listings_Stddev_Fields>;
    stddev_pop?: Maybe<App_Listings_Stddev_Pop_Fields>;
    stddev_samp?: Maybe<App_Listings_Stddev_Samp_Fields>;
    sum?: Maybe<App_Listings_Sum_Fields>;
    var_pop?: Maybe<App_Listings_Var_Pop_Fields>;
    var_samp?: Maybe<App_Listings_Var_Samp_Fields>;
    variance?: Maybe<App_Listings_Variance_Fields>;
};

/** aggregate fields of "app.listings" */
export type App_Listings_Aggregate_FieldsCountArgs = {
    columns?: InputMaybe<Array<App_Listings_Select_Column>>;
    distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "app.listings" */
export type App_Listings_Aggregate_Order_By = {
    avg?: InputMaybe<App_Listings_Avg_Order_By>;
    count?: InputMaybe<Order_By>;
    max?: InputMaybe<App_Listings_Max_Order_By>;
    min?: InputMaybe<App_Listings_Min_Order_By>;
    stddev?: InputMaybe<App_Listings_Stddev_Order_By>;
    stddev_pop?: InputMaybe<App_Listings_Stddev_Pop_Order_By>;
    stddev_samp?: InputMaybe<App_Listings_Stddev_Samp_Order_By>;
    sum?: InputMaybe<App_Listings_Sum_Order_By>;
    var_pop?: InputMaybe<App_Listings_Var_Pop_Order_By>;
    var_samp?: InputMaybe<App_Listings_Var_Samp_Order_By>;
    variance?: InputMaybe<App_Listings_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "app.listings" */
export type App_Listings_Arr_Rel_Insert_Input = {
    data: Array<App_Listings_Insert_Input>;
    /** upsert condition */
    on_conflict?: InputMaybe<App_Listings_On_Conflict>;
};

/** aggregate avg on columns */
export type App_Listings_Avg_Fields = {
    __typename?: 'app_listings_avg_fields';
    price?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "app.listings" */
export type App_Listings_Avg_Order_By = {
    price?: InputMaybe<Order_By>;
};

/** Boolean expression to filter rows from the table "app.listings". All fields are combined with a logical 'AND'. */
export type App_Listings_Bool_Exp = {
    _and?: InputMaybe<Array<App_Listings_Bool_Exp>>;
    _not?: InputMaybe<App_Listings_Bool_Exp>;
    _or?: InputMaybe<Array<App_Listings_Bool_Exp>>;
    created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
    creator?: InputMaybe<App_Users_Bool_Exp>;
    creator_id?: InputMaybe<Uuid_Comparison_Exp>;
    currency?: InputMaybe<Enums_Currency_Enum_Comparison_Exp>;
    description?: InputMaybe<String_Comparison_Exp>;
    id?: InputMaybe<Uuid_Comparison_Exp>;
    is_open?: InputMaybe<Boolean_Comparison_Exp>;
    is_selling?: InputMaybe<Boolean_Comparison_Exp>;
    photos?: InputMaybe<App_Photos_Bool_Exp>;
    price?: InputMaybe<Numeric_Comparison_Exp>;
    thumbnail?: InputMaybe<App_Photos_Bool_Exp>;
    thumbnail_id?: InputMaybe<Uuid_Comparison_Exp>;
    title?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "app.listings" */
export enum App_Listings_Constraint {
    /** unique or primary key constraint on columns "id" */
    ListingsPkey = 'listings_pkey',
}

/** input type for incrementing numeric columns in table "app.listings" */
export type App_Listings_Inc_Input = {
    price?: InputMaybe<Scalars['numeric']>;
};

/** input type for inserting data into table "app.listings" */
export type App_Listings_Insert_Input = {
    created_at?: InputMaybe<Scalars['timestamptz']>;
    creator?: InputMaybe<App_Users_Obj_Rel_Insert_Input>;
    creator_id?: InputMaybe<Scalars['uuid']>;
    currency?: InputMaybe<Enums_Currency_Enum>;
    description?: InputMaybe<Scalars['String']>;
    id?: InputMaybe<Scalars['uuid']>;
    is_open?: InputMaybe<Scalars['Boolean']>;
    is_selling?: InputMaybe<Scalars['Boolean']>;
    photos?: InputMaybe<App_Photos_Arr_Rel_Insert_Input>;
    price?: InputMaybe<Scalars['numeric']>;
    thumbnail?: InputMaybe<App_Photos_Obj_Rel_Insert_Input>;
    thumbnail_id?: InputMaybe<Scalars['uuid']>;
    title?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type App_Listings_Max_Fields = {
    __typename?: 'app_listings_max_fields';
    created_at?: Maybe<Scalars['timestamptz']>;
    creator_id?: Maybe<Scalars['uuid']>;
    description?: Maybe<Scalars['String']>;
    id?: Maybe<Scalars['uuid']>;
    price?: Maybe<Scalars['numeric']>;
    thumbnail_id?: Maybe<Scalars['uuid']>;
    title?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "app.listings" */
export type App_Listings_Max_Order_By = {
    created_at?: InputMaybe<Order_By>;
    creator_id?: InputMaybe<Order_By>;
    description?: InputMaybe<Order_By>;
    id?: InputMaybe<Order_By>;
    price?: InputMaybe<Order_By>;
    thumbnail_id?: InputMaybe<Order_By>;
    title?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type App_Listings_Min_Fields = {
    __typename?: 'app_listings_min_fields';
    created_at?: Maybe<Scalars['timestamptz']>;
    creator_id?: Maybe<Scalars['uuid']>;
    description?: Maybe<Scalars['String']>;
    id?: Maybe<Scalars['uuid']>;
    price?: Maybe<Scalars['numeric']>;
    thumbnail_id?: Maybe<Scalars['uuid']>;
    title?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "app.listings" */
export type App_Listings_Min_Order_By = {
    created_at?: InputMaybe<Order_By>;
    creator_id?: InputMaybe<Order_By>;
    description?: InputMaybe<Order_By>;
    id?: InputMaybe<Order_By>;
    price?: InputMaybe<Order_By>;
    thumbnail_id?: InputMaybe<Order_By>;
    title?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "app.listings" */
export type App_Listings_Mutation_Response = {
    __typename?: 'app_listings_mutation_response';
    /** number of rows affected by the mutation */
    affected_rows: Scalars['Int'];
    /** data from the rows affected by the mutation */
    returning: Array<App_Listings>;
};

/** input type for inserting object relation for remote table "app.listings" */
export type App_Listings_Obj_Rel_Insert_Input = {
    data: App_Listings_Insert_Input;
    /** upsert condition */
    on_conflict?: InputMaybe<App_Listings_On_Conflict>;
};

/** on_conflict condition type for table "app.listings" */
export type App_Listings_On_Conflict = {
    constraint: App_Listings_Constraint;
    update_columns?: Array<App_Listings_Update_Column>;
    where?: InputMaybe<App_Listings_Bool_Exp>;
};

/** Ordering options when selecting data from "app.listings". */
export type App_Listings_Order_By = {
    created_at?: InputMaybe<Order_By>;
    creator?: InputMaybe<App_Users_Order_By>;
    creator_id?: InputMaybe<Order_By>;
    currency?: InputMaybe<Order_By>;
    description?: InputMaybe<Order_By>;
    id?: InputMaybe<Order_By>;
    is_open?: InputMaybe<Order_By>;
    is_selling?: InputMaybe<Order_By>;
    photos_aggregate?: InputMaybe<App_Photos_Aggregate_Order_By>;
    price?: InputMaybe<Order_By>;
    thumbnail?: InputMaybe<App_Photos_Order_By>;
    thumbnail_id?: InputMaybe<Order_By>;
    title?: InputMaybe<Order_By>;
};

/** primary key columns input for table: app_listings */
export type App_Listings_Pk_Columns_Input = {
    id: Scalars['uuid'];
};

/** select columns of table "app.listings" */
export enum App_Listings_Select_Column {
    /** column name */
    CreatedAt = 'created_at',
    /** column name */
    CreatorId = 'creator_id',
    /** column name */
    Currency = 'currency',
    /** column name */
    Description = 'description',
    /** column name */
    Id = 'id',
    /** column name */
    IsOpen = 'is_open',
    /** column name */
    IsSelling = 'is_selling',
    /** column name */
    Price = 'price',
    /** column name */
    ThumbnailId = 'thumbnail_id',
    /** column name */
    Title = 'title',
}

/** input type for updating data in table "app.listings" */
export type App_Listings_Set_Input = {
    created_at?: InputMaybe<Scalars['timestamptz']>;
    creator_id?: InputMaybe<Scalars['uuid']>;
    currency?: InputMaybe<Enums_Currency_Enum>;
    description?: InputMaybe<Scalars['String']>;
    id?: InputMaybe<Scalars['uuid']>;
    is_open?: InputMaybe<Scalars['Boolean']>;
    is_selling?: InputMaybe<Scalars['Boolean']>;
    price?: InputMaybe<Scalars['numeric']>;
    thumbnail_id?: InputMaybe<Scalars['uuid']>;
    title?: InputMaybe<Scalars['String']>;
};

/** aggregate stddev on columns */
export type App_Listings_Stddev_Fields = {
    __typename?: 'app_listings_stddev_fields';
    price?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "app.listings" */
export type App_Listings_Stddev_Order_By = {
    price?: InputMaybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type App_Listings_Stddev_Pop_Fields = {
    __typename?: 'app_listings_stddev_pop_fields';
    price?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "app.listings" */
export type App_Listings_Stddev_Pop_Order_By = {
    price?: InputMaybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type App_Listings_Stddev_Samp_Fields = {
    __typename?: 'app_listings_stddev_samp_fields';
    price?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "app.listings" */
export type App_Listings_Stddev_Samp_Order_By = {
    price?: InputMaybe<Order_By>;
};

/** aggregate sum on columns */
export type App_Listings_Sum_Fields = {
    __typename?: 'app_listings_sum_fields';
    price?: Maybe<Scalars['numeric']>;
};

/** order by sum() on columns of table "app.listings" */
export type App_Listings_Sum_Order_By = {
    price?: InputMaybe<Order_By>;
};

/** update columns of table "app.listings" */
export enum App_Listings_Update_Column {
    /** column name */
    CreatedAt = 'created_at',
    /** column name */
    CreatorId = 'creator_id',
    /** column name */
    Currency = 'currency',
    /** column name */
    Description = 'description',
    /** column name */
    Id = 'id',
    /** column name */
    IsOpen = 'is_open',
    /** column name */
    IsSelling = 'is_selling',
    /** column name */
    Price = 'price',
    /** column name */
    ThumbnailId = 'thumbnail_id',
    /** column name */
    Title = 'title',
}

/** aggregate var_pop on columns */
export type App_Listings_Var_Pop_Fields = {
    __typename?: 'app_listings_var_pop_fields';
    price?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "app.listings" */
export type App_Listings_Var_Pop_Order_By = {
    price?: InputMaybe<Order_By>;
};

/** aggregate var_samp on columns */
export type App_Listings_Var_Samp_Fields = {
    __typename?: 'app_listings_var_samp_fields';
    price?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "app.listings" */
export type App_Listings_Var_Samp_Order_By = {
    price?: InputMaybe<Order_By>;
};

/** aggregate variance on columns */
export type App_Listings_Variance_Fields = {
    __typename?: 'app_listings_variance_fields';
    price?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "app.listings" */
export type App_Listings_Variance_Order_By = {
    price?: InputMaybe<Order_By>;
};

/** 1-to-many in relation to listings */
export type App_Photos = {
    __typename?: 'app_photos';
    id: Scalars['uuid'];
    link: Scalars['String'];
    /** An object relationship */
    listing: App_Listings;
    listing_id: Scalars['uuid'];
};

/** aggregated selection of "app.photos" */
export type App_Photos_Aggregate = {
    __typename?: 'app_photos_aggregate';
    aggregate?: Maybe<App_Photos_Aggregate_Fields>;
    nodes: Array<App_Photos>;
};

/** aggregate fields of "app.photos" */
export type App_Photos_Aggregate_Fields = {
    __typename?: 'app_photos_aggregate_fields';
    count: Scalars['Int'];
    max?: Maybe<App_Photos_Max_Fields>;
    min?: Maybe<App_Photos_Min_Fields>;
};

/** aggregate fields of "app.photos" */
export type App_Photos_Aggregate_FieldsCountArgs = {
    columns?: InputMaybe<Array<App_Photos_Select_Column>>;
    distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "app.photos" */
export type App_Photos_Aggregate_Order_By = {
    count?: InputMaybe<Order_By>;
    max?: InputMaybe<App_Photos_Max_Order_By>;
    min?: InputMaybe<App_Photos_Min_Order_By>;
};

/** input type for inserting array relation for remote table "app.photos" */
export type App_Photos_Arr_Rel_Insert_Input = {
    data: Array<App_Photos_Insert_Input>;
    /** upsert condition */
    on_conflict?: InputMaybe<App_Photos_On_Conflict>;
};

/** Boolean expression to filter rows from the table "app.photos". All fields are combined with a logical 'AND'. */
export type App_Photos_Bool_Exp = {
    _and?: InputMaybe<Array<App_Photos_Bool_Exp>>;
    _not?: InputMaybe<App_Photos_Bool_Exp>;
    _or?: InputMaybe<Array<App_Photos_Bool_Exp>>;
    id?: InputMaybe<Uuid_Comparison_Exp>;
    link?: InputMaybe<String_Comparison_Exp>;
    listing?: InputMaybe<App_Listings_Bool_Exp>;
    listing_id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "app.photos" */
export enum App_Photos_Constraint {
    /** unique or primary key constraint on columns "id" */
    PhotosPkey = 'photos_pkey',
}

/** input type for inserting data into table "app.photos" */
export type App_Photos_Insert_Input = {
    id?: InputMaybe<Scalars['uuid']>;
    link?: InputMaybe<Scalars['String']>;
    listing?: InputMaybe<App_Listings_Obj_Rel_Insert_Input>;
    listing_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type App_Photos_Max_Fields = {
    __typename?: 'app_photos_max_fields';
    id?: Maybe<Scalars['uuid']>;
    link?: Maybe<Scalars['String']>;
    listing_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "app.photos" */
export type App_Photos_Max_Order_By = {
    id?: InputMaybe<Order_By>;
    link?: InputMaybe<Order_By>;
    listing_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type App_Photos_Min_Fields = {
    __typename?: 'app_photos_min_fields';
    id?: Maybe<Scalars['uuid']>;
    link?: Maybe<Scalars['String']>;
    listing_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "app.photos" */
export type App_Photos_Min_Order_By = {
    id?: InputMaybe<Order_By>;
    link?: InputMaybe<Order_By>;
    listing_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "app.photos" */
export type App_Photos_Mutation_Response = {
    __typename?: 'app_photos_mutation_response';
    /** number of rows affected by the mutation */
    affected_rows: Scalars['Int'];
    /** data from the rows affected by the mutation */
    returning: Array<App_Photos>;
};

/** input type for inserting object relation for remote table "app.photos" */
export type App_Photos_Obj_Rel_Insert_Input = {
    data: App_Photos_Insert_Input;
    /** upsert condition */
    on_conflict?: InputMaybe<App_Photos_On_Conflict>;
};

/** on_conflict condition type for table "app.photos" */
export type App_Photos_On_Conflict = {
    constraint: App_Photos_Constraint;
    update_columns?: Array<App_Photos_Update_Column>;
    where?: InputMaybe<App_Photos_Bool_Exp>;
};

/** Ordering options when selecting data from "app.photos". */
export type App_Photos_Order_By = {
    id?: InputMaybe<Order_By>;
    link?: InputMaybe<Order_By>;
    listing?: InputMaybe<App_Listings_Order_By>;
    listing_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: app_photos */
export type App_Photos_Pk_Columns_Input = {
    id: Scalars['uuid'];
};

/** select columns of table "app.photos" */
export enum App_Photos_Select_Column {
    /** column name */
    Id = 'id',
    /** column name */
    Link = 'link',
    /** column name */
    ListingId = 'listing_id',
}

/** input type for updating data in table "app.photos" */
export type App_Photos_Set_Input = {
    id?: InputMaybe<Scalars['uuid']>;
    link?: InputMaybe<Scalars['String']>;
    listing_id?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "app.photos" */
export enum App_Photos_Update_Column {
    /** column name */
    Id = 'id',
    /** column name */
    Link = 'link',
    /** column name */
    ListingId = 'listing_id',
}

/** Global app users */
export type App_Users = {
    __typename?: 'app_users';
    email: Scalars['String'];
    id: Scalars['uuid'];
    /** An array relationship */
    listings: Array<App_Listings>;
    /** An aggregate relationship */
    listings_aggregate: App_Listings_Aggregate;
    name?: Maybe<Scalars['String']>;
};

/** Global app users */
export type App_UsersListingsArgs = {
    distinct_on?: InputMaybe<Array<App_Listings_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Listings_Order_By>>;
    where?: InputMaybe<App_Listings_Bool_Exp>;
};

/** Global app users */
export type App_UsersListings_AggregateArgs = {
    distinct_on?: InputMaybe<Array<App_Listings_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Listings_Order_By>>;
    where?: InputMaybe<App_Listings_Bool_Exp>;
};

/** aggregated selection of "app.users" */
export type App_Users_Aggregate = {
    __typename?: 'app_users_aggregate';
    aggregate?: Maybe<App_Users_Aggregate_Fields>;
    nodes: Array<App_Users>;
};

/** aggregate fields of "app.users" */
export type App_Users_Aggregate_Fields = {
    __typename?: 'app_users_aggregate_fields';
    count: Scalars['Int'];
    max?: Maybe<App_Users_Max_Fields>;
    min?: Maybe<App_Users_Min_Fields>;
};

/** aggregate fields of "app.users" */
export type App_Users_Aggregate_FieldsCountArgs = {
    columns?: InputMaybe<Array<App_Users_Select_Column>>;
    distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "app.users". All fields are combined with a logical 'AND'. */
export type App_Users_Bool_Exp = {
    _and?: InputMaybe<Array<App_Users_Bool_Exp>>;
    _not?: InputMaybe<App_Users_Bool_Exp>;
    _or?: InputMaybe<Array<App_Users_Bool_Exp>>;
    email?: InputMaybe<String_Comparison_Exp>;
    id?: InputMaybe<Uuid_Comparison_Exp>;
    listings?: InputMaybe<App_Listings_Bool_Exp>;
    name?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "app.users" */
export enum App_Users_Constraint {
    /** unique or primary key constraint on columns "email" */
    UsersEmailKey = 'users_email_key',
    /** unique or primary key constraint on columns "name" */
    UsersNameKey = 'users_name_key',
    /** unique or primary key constraint on columns "id" */
    UsersPkey = 'users_pkey',
}

/** input type for inserting data into table "app.users" */
export type App_Users_Insert_Input = {
    email?: InputMaybe<Scalars['String']>;
    id?: InputMaybe<Scalars['uuid']>;
    listings?: InputMaybe<App_Listings_Arr_Rel_Insert_Input>;
    name?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type App_Users_Max_Fields = {
    __typename?: 'app_users_max_fields';
    email?: Maybe<Scalars['String']>;
    id?: Maybe<Scalars['uuid']>;
    name?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type App_Users_Min_Fields = {
    __typename?: 'app_users_min_fields';
    email?: Maybe<Scalars['String']>;
    id?: Maybe<Scalars['uuid']>;
    name?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "app.users" */
export type App_Users_Mutation_Response = {
    __typename?: 'app_users_mutation_response';
    /** number of rows affected by the mutation */
    affected_rows: Scalars['Int'];
    /** data from the rows affected by the mutation */
    returning: Array<App_Users>;
};

/** input type for inserting object relation for remote table "app.users" */
export type App_Users_Obj_Rel_Insert_Input = {
    data: App_Users_Insert_Input;
    /** upsert condition */
    on_conflict?: InputMaybe<App_Users_On_Conflict>;
};

/** on_conflict condition type for table "app.users" */
export type App_Users_On_Conflict = {
    constraint: App_Users_Constraint;
    update_columns?: Array<App_Users_Update_Column>;
    where?: InputMaybe<App_Users_Bool_Exp>;
};

/** Ordering options when selecting data from "app.users". */
export type App_Users_Order_By = {
    email?: InputMaybe<Order_By>;
    id?: InputMaybe<Order_By>;
    listings_aggregate?: InputMaybe<App_Listings_Aggregate_Order_By>;
    name?: InputMaybe<Order_By>;
};

/** primary key columns input for table: app_users */
export type App_Users_Pk_Columns_Input = {
    id: Scalars['uuid'];
};

/** select columns of table "app.users" */
export enum App_Users_Select_Column {
    /** column name */
    Email = 'email',
    /** column name */
    Id = 'id',
    /** column name */
    Name = 'name',
}

/** input type for updating data in table "app.users" */
export type App_Users_Set_Input = {
    email?: InputMaybe<Scalars['String']>;
    id?: InputMaybe<Scalars['uuid']>;
    name?: InputMaybe<Scalars['String']>;
};

/** update columns of table "app.users" */
export enum App_Users_Update_Column {
    /** column name */
    Email = 'email',
    /** column name */
    Id = 'id',
    /** column name */
    Name = 'name',
}

/** Supported currencies */
export type Enums_Currency = {
    __typename?: 'enums_currency';
    comment?: Maybe<Scalars['String']>;
    value: Scalars['String'];
};

/** aggregated selection of "enums.currency" */
export type Enums_Currency_Aggregate = {
    __typename?: 'enums_currency_aggregate';
    aggregate?: Maybe<Enums_Currency_Aggregate_Fields>;
    nodes: Array<Enums_Currency>;
};

/** aggregate fields of "enums.currency" */
export type Enums_Currency_Aggregate_Fields = {
    __typename?: 'enums_currency_aggregate_fields';
    count: Scalars['Int'];
    max?: Maybe<Enums_Currency_Max_Fields>;
    min?: Maybe<Enums_Currency_Min_Fields>;
};

/** aggregate fields of "enums.currency" */
export type Enums_Currency_Aggregate_FieldsCountArgs = {
    columns?: InputMaybe<Array<Enums_Currency_Select_Column>>;
    distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "enums.currency". All fields are combined with a logical 'AND'. */
export type Enums_Currency_Bool_Exp = {
    _and?: InputMaybe<Array<Enums_Currency_Bool_Exp>>;
    _not?: InputMaybe<Enums_Currency_Bool_Exp>;
    _or?: InputMaybe<Array<Enums_Currency_Bool_Exp>>;
    comment?: InputMaybe<String_Comparison_Exp>;
    value?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "enums.currency" */
export enum Enums_Currency_Constraint {
    /** unique or primary key constraint on columns "value" */
    CurrencyPkey = 'currency_pkey',
}

export enum Enums_Currency_Enum {
    /** Czech koruna */
    Czk = 'czk',
}

/** Boolean expression to compare columns of type "enums_currency_enum". All fields are combined with logical 'AND'. */
export type Enums_Currency_Enum_Comparison_Exp = {
    _eq?: InputMaybe<Enums_Currency_Enum>;
    _in?: InputMaybe<Array<Enums_Currency_Enum>>;
    _is_null?: InputMaybe<Scalars['Boolean']>;
    _neq?: InputMaybe<Enums_Currency_Enum>;
    _nin?: InputMaybe<Array<Enums_Currency_Enum>>;
};

/** input type for inserting data into table "enums.currency" */
export type Enums_Currency_Insert_Input = {
    comment?: InputMaybe<Scalars['String']>;
    value?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Enums_Currency_Max_Fields = {
    __typename?: 'enums_currency_max_fields';
    comment?: Maybe<Scalars['String']>;
    value?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Enums_Currency_Min_Fields = {
    __typename?: 'enums_currency_min_fields';
    comment?: Maybe<Scalars['String']>;
    value?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "enums.currency" */
export type Enums_Currency_Mutation_Response = {
    __typename?: 'enums_currency_mutation_response';
    /** number of rows affected by the mutation */
    affected_rows: Scalars['Int'];
    /** data from the rows affected by the mutation */
    returning: Array<Enums_Currency>;
};

/** on_conflict condition type for table "enums.currency" */
export type Enums_Currency_On_Conflict = {
    constraint: Enums_Currency_Constraint;
    update_columns?: Array<Enums_Currency_Update_Column>;
    where?: InputMaybe<Enums_Currency_Bool_Exp>;
};

/** Ordering options when selecting data from "enums.currency". */
export type Enums_Currency_Order_By = {
    comment?: InputMaybe<Order_By>;
    value?: InputMaybe<Order_By>;
};

/** primary key columns input for table: enums_currency */
export type Enums_Currency_Pk_Columns_Input = {
    value: Scalars['String'];
};

/** select columns of table "enums.currency" */
export enum Enums_Currency_Select_Column {
    /** column name */
    Comment = 'comment',
    /** column name */
    Value = 'value',
}

/** input type for updating data in table "enums.currency" */
export type Enums_Currency_Set_Input = {
    comment?: InputMaybe<Scalars['String']>;
    value?: InputMaybe<Scalars['String']>;
};

/** update columns of table "enums.currency" */
export enum Enums_Currency_Update_Column {
    /** column name */
    Comment = 'comment',
    /** column name */
    Value = 'value',
}

/** Supported listing types */
export type Enums_Listing_Type = {
    __typename?: 'enums_listing_type';
    comment?: Maybe<Scalars['String']>;
    value: Scalars['String'];
};

/** aggregated selection of "enums.listing_type" */
export type Enums_Listing_Type_Aggregate = {
    __typename?: 'enums_listing_type_aggregate';
    aggregate?: Maybe<Enums_Listing_Type_Aggregate_Fields>;
    nodes: Array<Enums_Listing_Type>;
};

/** aggregate fields of "enums.listing_type" */
export type Enums_Listing_Type_Aggregate_Fields = {
    __typename?: 'enums_listing_type_aggregate_fields';
    count: Scalars['Int'];
    max?: Maybe<Enums_Listing_Type_Max_Fields>;
    min?: Maybe<Enums_Listing_Type_Min_Fields>;
};

/** aggregate fields of "enums.listing_type" */
export type Enums_Listing_Type_Aggregate_FieldsCountArgs = {
    columns?: InputMaybe<Array<Enums_Listing_Type_Select_Column>>;
    distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "enums.listing_type". All fields are combined with a logical 'AND'. */
export type Enums_Listing_Type_Bool_Exp = {
    _and?: InputMaybe<Array<Enums_Listing_Type_Bool_Exp>>;
    _not?: InputMaybe<Enums_Listing_Type_Bool_Exp>;
    _or?: InputMaybe<Array<Enums_Listing_Type_Bool_Exp>>;
    comment?: InputMaybe<String_Comparison_Exp>;
    value?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "enums.listing_type" */
export enum Enums_Listing_Type_Constraint {
    /** unique or primary key constraint on columns "value" */
    ListingTypePkey = 'listing_type_pkey',
}

/** input type for inserting data into table "enums.listing_type" */
export type Enums_Listing_Type_Insert_Input = {
    comment?: InputMaybe<Scalars['String']>;
    value?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Enums_Listing_Type_Max_Fields = {
    __typename?: 'enums_listing_type_max_fields';
    comment?: Maybe<Scalars['String']>;
    value?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Enums_Listing_Type_Min_Fields = {
    __typename?: 'enums_listing_type_min_fields';
    comment?: Maybe<Scalars['String']>;
    value?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "enums.listing_type" */
export type Enums_Listing_Type_Mutation_Response = {
    __typename?: 'enums_listing_type_mutation_response';
    /** number of rows affected by the mutation */
    affected_rows: Scalars['Int'];
    /** data from the rows affected by the mutation */
    returning: Array<Enums_Listing_Type>;
};

/** on_conflict condition type for table "enums.listing_type" */
export type Enums_Listing_Type_On_Conflict = {
    constraint: Enums_Listing_Type_Constraint;
    update_columns?: Array<Enums_Listing_Type_Update_Column>;
    where?: InputMaybe<Enums_Listing_Type_Bool_Exp>;
};

/** Ordering options when selecting data from "enums.listing_type". */
export type Enums_Listing_Type_Order_By = {
    comment?: InputMaybe<Order_By>;
    value?: InputMaybe<Order_By>;
};

/** primary key columns input for table: enums_listing_type */
export type Enums_Listing_Type_Pk_Columns_Input = {
    value: Scalars['String'];
};

/** select columns of table "enums.listing_type" */
export enum Enums_Listing_Type_Select_Column {
    /** column name */
    Comment = 'comment',
    /** column name */
    Value = 'value',
}

/** input type for updating data in table "enums.listing_type" */
export type Enums_Listing_Type_Set_Input = {
    comment?: InputMaybe<Scalars['String']>;
    value?: InputMaybe<Scalars['String']>;
};

/** update columns of table "enums.listing_type" */
export enum Enums_Listing_Type_Update_Column {
    /** column name */
    Comment = 'comment',
    /** column name */
    Value = 'value',
}

/** mutation root */
export type Mutation_Root = {
    __typename?: 'mutation_root';
    /** delete data from the table: "app.listings" */
    delete_app_listings?: Maybe<App_Listings_Mutation_Response>;
    /** delete single row from the table: "app.listings" */
    delete_app_listings_by_pk?: Maybe<App_Listings>;
    /** delete data from the table: "app.photos" */
    delete_app_photos?: Maybe<App_Photos_Mutation_Response>;
    /** delete single row from the table: "app.photos" */
    delete_app_photos_by_pk?: Maybe<App_Photos>;
    /** delete data from the table: "app.users" */
    delete_app_users?: Maybe<App_Users_Mutation_Response>;
    /** delete single row from the table: "app.users" */
    delete_app_users_by_pk?: Maybe<App_Users>;
    /** delete data from the table: "enums.currency" */
    delete_enums_currency?: Maybe<Enums_Currency_Mutation_Response>;
    /** delete single row from the table: "enums.currency" */
    delete_enums_currency_by_pk?: Maybe<Enums_Currency>;
    /** delete data from the table: "enums.listing_type" */
    delete_enums_listing_type?: Maybe<Enums_Listing_Type_Mutation_Response>;
    /** delete single row from the table: "enums.listing_type" */
    delete_enums_listing_type_by_pk?: Maybe<Enums_Listing_Type>;
    /** insert data into the table: "app.listings" */
    insert_app_listings?: Maybe<App_Listings_Mutation_Response>;
    /** insert a single row into the table: "app.listings" */
    insert_app_listings_one?: Maybe<App_Listings>;
    /** insert data into the table: "app.photos" */
    insert_app_photos?: Maybe<App_Photos_Mutation_Response>;
    /** insert a single row into the table: "app.photos" */
    insert_app_photos_one?: Maybe<App_Photos>;
    /** insert data into the table: "app.users" */
    insert_app_users?: Maybe<App_Users_Mutation_Response>;
    /** insert a single row into the table: "app.users" */
    insert_app_users_one?: Maybe<App_Users>;
    /** insert data into the table: "enums.currency" */
    insert_enums_currency?: Maybe<Enums_Currency_Mutation_Response>;
    /** insert a single row into the table: "enums.currency" */
    insert_enums_currency_one?: Maybe<Enums_Currency>;
    /** insert data into the table: "enums.listing_type" */
    insert_enums_listing_type?: Maybe<Enums_Listing_Type_Mutation_Response>;
    /** insert a single row into the table: "enums.listing_type" */
    insert_enums_listing_type_one?: Maybe<Enums_Listing_Type>;
    /** update data of the table: "app.listings" */
    update_app_listings?: Maybe<App_Listings_Mutation_Response>;
    /** update single row of the table: "app.listings" */
    update_app_listings_by_pk?: Maybe<App_Listings>;
    /** update data of the table: "app.photos" */
    update_app_photos?: Maybe<App_Photos_Mutation_Response>;
    /** update single row of the table: "app.photos" */
    update_app_photos_by_pk?: Maybe<App_Photos>;
    /** update data of the table: "app.users" */
    update_app_users?: Maybe<App_Users_Mutation_Response>;
    /** update single row of the table: "app.users" */
    update_app_users_by_pk?: Maybe<App_Users>;
    /** update data of the table: "enums.currency" */
    update_enums_currency?: Maybe<Enums_Currency_Mutation_Response>;
    /** update single row of the table: "enums.currency" */
    update_enums_currency_by_pk?: Maybe<Enums_Currency>;
    /** update data of the table: "enums.listing_type" */
    update_enums_listing_type?: Maybe<Enums_Listing_Type_Mutation_Response>;
    /** update single row of the table: "enums.listing_type" */
    update_enums_listing_type_by_pk?: Maybe<Enums_Listing_Type>;
    uploadPublicFile?: Maybe<FileResource>;
};

/** mutation root */
export type Mutation_RootDelete_App_ListingsArgs = {
    where: App_Listings_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_App_Listings_By_PkArgs = {
    id: Scalars['uuid'];
};

/** mutation root */
export type Mutation_RootDelete_App_PhotosArgs = {
    where: App_Photos_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_App_Photos_By_PkArgs = {
    id: Scalars['uuid'];
};

/** mutation root */
export type Mutation_RootDelete_App_UsersArgs = {
    where: App_Users_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_App_Users_By_PkArgs = {
    id: Scalars['uuid'];
};

/** mutation root */
export type Mutation_RootDelete_Enums_CurrencyArgs = {
    where: Enums_Currency_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_Enums_Currency_By_PkArgs = {
    value: Scalars['String'];
};

/** mutation root */
export type Mutation_RootDelete_Enums_Listing_TypeArgs = {
    where: Enums_Listing_Type_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_Enums_Listing_Type_By_PkArgs = {
    value: Scalars['String'];
};

/** mutation root */
export type Mutation_RootInsert_App_ListingsArgs = {
    objects: Array<App_Listings_Insert_Input>;
    on_conflict?: InputMaybe<App_Listings_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_App_Listings_OneArgs = {
    object: App_Listings_Insert_Input;
    on_conflict?: InputMaybe<App_Listings_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_App_PhotosArgs = {
    objects: Array<App_Photos_Insert_Input>;
    on_conflict?: InputMaybe<App_Photos_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_App_Photos_OneArgs = {
    object: App_Photos_Insert_Input;
    on_conflict?: InputMaybe<App_Photos_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_App_UsersArgs = {
    objects: Array<App_Users_Insert_Input>;
    on_conflict?: InputMaybe<App_Users_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_App_Users_OneArgs = {
    object: App_Users_Insert_Input;
    on_conflict?: InputMaybe<App_Users_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_Enums_CurrencyArgs = {
    objects: Array<Enums_Currency_Insert_Input>;
    on_conflict?: InputMaybe<Enums_Currency_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_Enums_Currency_OneArgs = {
    object: Enums_Currency_Insert_Input;
    on_conflict?: InputMaybe<Enums_Currency_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_Enums_Listing_TypeArgs = {
    objects: Array<Enums_Listing_Type_Insert_Input>;
    on_conflict?: InputMaybe<Enums_Listing_Type_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_Enums_Listing_Type_OneArgs = {
    object: Enums_Listing_Type_Insert_Input;
    on_conflict?: InputMaybe<Enums_Listing_Type_On_Conflict>;
};

/** mutation root */
export type Mutation_RootUpdate_App_ListingsArgs = {
    _inc?: InputMaybe<App_Listings_Inc_Input>;
    _set?: InputMaybe<App_Listings_Set_Input>;
    where: App_Listings_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_App_Listings_By_PkArgs = {
    _inc?: InputMaybe<App_Listings_Inc_Input>;
    _set?: InputMaybe<App_Listings_Set_Input>;
    pk_columns: App_Listings_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUpdate_App_PhotosArgs = {
    _set?: InputMaybe<App_Photos_Set_Input>;
    where: App_Photos_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_App_Photos_By_PkArgs = {
    _set?: InputMaybe<App_Photos_Set_Input>;
    pk_columns: App_Photos_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUpdate_App_UsersArgs = {
    _set?: InputMaybe<App_Users_Set_Input>;
    where: App_Users_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_App_Users_By_PkArgs = {
    _set?: InputMaybe<App_Users_Set_Input>;
    pk_columns: App_Users_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUpdate_Enums_CurrencyArgs = {
    _set?: InputMaybe<Enums_Currency_Set_Input>;
    where: Enums_Currency_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_Enums_Currency_By_PkArgs = {
    _set?: InputMaybe<Enums_Currency_Set_Input>;
    pk_columns: Enums_Currency_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUpdate_Enums_Listing_TypeArgs = {
    _set?: InputMaybe<Enums_Listing_Type_Set_Input>;
    where: Enums_Listing_Type_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_Enums_Listing_Type_By_PkArgs = {
    _set?: InputMaybe<Enums_Listing_Type_Set_Input>;
    pk_columns: Enums_Listing_Type_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUploadPublicFileArgs = {
    file: UploadPublicFileInput;
};

export type Numeric_Cast_Exp = {
    String?: InputMaybe<String_Comparison_Exp>;
};

/** Boolean expression to compare columns of type "numeric". All fields are combined with logical 'AND'. */
export type Numeric_Comparison_Exp = {
    _cast?: InputMaybe<Numeric_Cast_Exp>;
    _eq?: InputMaybe<Scalars['numeric']>;
    _gt?: InputMaybe<Scalars['numeric']>;
    _gte?: InputMaybe<Scalars['numeric']>;
    _in?: InputMaybe<Array<Scalars['numeric']>>;
    _is_null?: InputMaybe<Scalars['Boolean']>;
    _lt?: InputMaybe<Scalars['numeric']>;
    _lte?: InputMaybe<Scalars['numeric']>;
    _neq?: InputMaybe<Scalars['numeric']>;
    _nin?: InputMaybe<Array<Scalars['numeric']>>;
};

/** column ordering options */
export enum Order_By {
    /** in ascending order, nulls last */
    Asc = 'asc',
    /** in ascending order, nulls first */
    AscNullsFirst = 'asc_nulls_first',
    /** in ascending order, nulls last */
    AscNullsLast = 'asc_nulls_last',
    /** in descending order, nulls first */
    Desc = 'desc',
    /** in descending order, nulls first */
    DescNullsFirst = 'desc_nulls_first',
    /** in descending order, nulls last */
    DescNullsLast = 'desc_nulls_last',
}

export type Query_Root = {
    __typename?: 'query_root';
    /** fetch data from the table: "app.listings" */
    app_listings: Array<App_Listings>;
    /** fetch aggregated fields from the table: "app.listings" */
    app_listings_aggregate: App_Listings_Aggregate;
    /** fetch data from the table: "app.listings" using primary key columns */
    app_listings_by_pk?: Maybe<App_Listings>;
    /** fetch data from the table: "app.photos" */
    app_photos: Array<App_Photos>;
    /** fetch aggregated fields from the table: "app.photos" */
    app_photos_aggregate: App_Photos_Aggregate;
    /** fetch data from the table: "app.photos" using primary key columns */
    app_photos_by_pk?: Maybe<App_Photos>;
    /** fetch data from the table: "app.users" */
    app_users: Array<App_Users>;
    /** fetch aggregated fields from the table: "app.users" */
    app_users_aggregate: App_Users_Aggregate;
    /** fetch data from the table: "app.users" using primary key columns */
    app_users_by_pk?: Maybe<App_Users>;
    /** fetch data from the table: "enums.currency" */
    enums_currency: Array<Enums_Currency>;
    /** fetch aggregated fields from the table: "enums.currency" */
    enums_currency_aggregate: Enums_Currency_Aggregate;
    /** fetch data from the table: "enums.currency" using primary key columns */
    enums_currency_by_pk?: Maybe<Enums_Currency>;
    /** fetch data from the table: "enums.listing_type" */
    enums_listing_type: Array<Enums_Listing_Type>;
    /** fetch aggregated fields from the table: "enums.listing_type" */
    enums_listing_type_aggregate: Enums_Listing_Type_Aggregate;
    /** fetch data from the table: "enums.listing_type" using primary key columns */
    enums_listing_type_by_pk?: Maybe<Enums_Listing_Type>;
};

export type Query_RootApp_ListingsArgs = {
    distinct_on?: InputMaybe<Array<App_Listings_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Listings_Order_By>>;
    where?: InputMaybe<App_Listings_Bool_Exp>;
};

export type Query_RootApp_Listings_AggregateArgs = {
    distinct_on?: InputMaybe<Array<App_Listings_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Listings_Order_By>>;
    where?: InputMaybe<App_Listings_Bool_Exp>;
};

export type Query_RootApp_Listings_By_PkArgs = {
    id: Scalars['uuid'];
};

export type Query_RootApp_PhotosArgs = {
    distinct_on?: InputMaybe<Array<App_Photos_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Photos_Order_By>>;
    where?: InputMaybe<App_Photos_Bool_Exp>;
};

export type Query_RootApp_Photos_AggregateArgs = {
    distinct_on?: InputMaybe<Array<App_Photos_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Photos_Order_By>>;
    where?: InputMaybe<App_Photos_Bool_Exp>;
};

export type Query_RootApp_Photos_By_PkArgs = {
    id: Scalars['uuid'];
};

export type Query_RootApp_UsersArgs = {
    distinct_on?: InputMaybe<Array<App_Users_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Users_Order_By>>;
    where?: InputMaybe<App_Users_Bool_Exp>;
};

export type Query_RootApp_Users_AggregateArgs = {
    distinct_on?: InputMaybe<Array<App_Users_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Users_Order_By>>;
    where?: InputMaybe<App_Users_Bool_Exp>;
};

export type Query_RootApp_Users_By_PkArgs = {
    id: Scalars['uuid'];
};

export type Query_RootEnums_CurrencyArgs = {
    distinct_on?: InputMaybe<Array<Enums_Currency_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<Enums_Currency_Order_By>>;
    where?: InputMaybe<Enums_Currency_Bool_Exp>;
};

export type Query_RootEnums_Currency_AggregateArgs = {
    distinct_on?: InputMaybe<Array<Enums_Currency_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<Enums_Currency_Order_By>>;
    where?: InputMaybe<Enums_Currency_Bool_Exp>;
};

export type Query_RootEnums_Currency_By_PkArgs = {
    value: Scalars['String'];
};

export type Query_RootEnums_Listing_TypeArgs = {
    distinct_on?: InputMaybe<Array<Enums_Listing_Type_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<Enums_Listing_Type_Order_By>>;
    where?: InputMaybe<Enums_Listing_Type_Bool_Exp>;
};

export type Query_RootEnums_Listing_Type_AggregateArgs = {
    distinct_on?: InputMaybe<Array<Enums_Listing_Type_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<Enums_Listing_Type_Order_By>>;
    where?: InputMaybe<Enums_Listing_Type_Bool_Exp>;
};

export type Query_RootEnums_Listing_Type_By_PkArgs = {
    value: Scalars['String'];
};

export type Subscription_Root = {
    __typename?: 'subscription_root';
    /** fetch data from the table: "app.listings" */
    app_listings: Array<App_Listings>;
    /** fetch aggregated fields from the table: "app.listings" */
    app_listings_aggregate: App_Listings_Aggregate;
    /** fetch data from the table: "app.listings" using primary key columns */
    app_listings_by_pk?: Maybe<App_Listings>;
    /** fetch data from the table: "app.photos" */
    app_photos: Array<App_Photos>;
    /** fetch aggregated fields from the table: "app.photos" */
    app_photos_aggregate: App_Photos_Aggregate;
    /** fetch data from the table: "app.photos" using primary key columns */
    app_photos_by_pk?: Maybe<App_Photos>;
    /** fetch data from the table: "app.users" */
    app_users: Array<App_Users>;
    /** fetch aggregated fields from the table: "app.users" */
    app_users_aggregate: App_Users_Aggregate;
    /** fetch data from the table: "app.users" using primary key columns */
    app_users_by_pk?: Maybe<App_Users>;
    /** fetch data from the table: "enums.currency" */
    enums_currency: Array<Enums_Currency>;
    /** fetch aggregated fields from the table: "enums.currency" */
    enums_currency_aggregate: Enums_Currency_Aggregate;
    /** fetch data from the table: "enums.currency" using primary key columns */
    enums_currency_by_pk?: Maybe<Enums_Currency>;
    /** fetch data from the table: "enums.listing_type" */
    enums_listing_type: Array<Enums_Listing_Type>;
    /** fetch aggregated fields from the table: "enums.listing_type" */
    enums_listing_type_aggregate: Enums_Listing_Type_Aggregate;
    /** fetch data from the table: "enums.listing_type" using primary key columns */
    enums_listing_type_by_pk?: Maybe<Enums_Listing_Type>;
};

export type Subscription_RootApp_ListingsArgs = {
    distinct_on?: InputMaybe<Array<App_Listings_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Listings_Order_By>>;
    where?: InputMaybe<App_Listings_Bool_Exp>;
};

export type Subscription_RootApp_Listings_AggregateArgs = {
    distinct_on?: InputMaybe<Array<App_Listings_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Listings_Order_By>>;
    where?: InputMaybe<App_Listings_Bool_Exp>;
};

export type Subscription_RootApp_Listings_By_PkArgs = {
    id: Scalars['uuid'];
};

export type Subscription_RootApp_PhotosArgs = {
    distinct_on?: InputMaybe<Array<App_Photos_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Photos_Order_By>>;
    where?: InputMaybe<App_Photos_Bool_Exp>;
};

export type Subscription_RootApp_Photos_AggregateArgs = {
    distinct_on?: InputMaybe<Array<App_Photos_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Photos_Order_By>>;
    where?: InputMaybe<App_Photos_Bool_Exp>;
};

export type Subscription_RootApp_Photos_By_PkArgs = {
    id: Scalars['uuid'];
};

export type Subscription_RootApp_UsersArgs = {
    distinct_on?: InputMaybe<Array<App_Users_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Users_Order_By>>;
    where?: InputMaybe<App_Users_Bool_Exp>;
};

export type Subscription_RootApp_Users_AggregateArgs = {
    distinct_on?: InputMaybe<Array<App_Users_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<App_Users_Order_By>>;
    where?: InputMaybe<App_Users_Bool_Exp>;
};

export type Subscription_RootApp_Users_By_PkArgs = {
    id: Scalars['uuid'];
};

export type Subscription_RootEnums_CurrencyArgs = {
    distinct_on?: InputMaybe<Array<Enums_Currency_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<Enums_Currency_Order_By>>;
    where?: InputMaybe<Enums_Currency_Bool_Exp>;
};

export type Subscription_RootEnums_Currency_AggregateArgs = {
    distinct_on?: InputMaybe<Array<Enums_Currency_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<Enums_Currency_Order_By>>;
    where?: InputMaybe<Enums_Currency_Bool_Exp>;
};

export type Subscription_RootEnums_Currency_By_PkArgs = {
    value: Scalars['String'];
};

export type Subscription_RootEnums_Listing_TypeArgs = {
    distinct_on?: InputMaybe<Array<Enums_Listing_Type_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<Enums_Listing_Type_Order_By>>;
    where?: InputMaybe<Enums_Listing_Type_Bool_Exp>;
};

export type Subscription_RootEnums_Listing_Type_AggregateArgs = {
    distinct_on?: InputMaybe<Array<Enums_Listing_Type_Select_Column>>;
    limit?: InputMaybe<Scalars['Int']>;
    offset?: InputMaybe<Scalars['Int']>;
    order_by?: InputMaybe<Array<Enums_Listing_Type_Order_By>>;
    where?: InputMaybe<Enums_Listing_Type_Bool_Exp>;
};

export type Subscription_RootEnums_Listing_Type_By_PkArgs = {
    value: Scalars['String'];
};

export type Timestamptz_Cast_Exp = {
    String?: InputMaybe<String_Comparison_Exp>;
};

/** Boolean expression to compare columns of type "timestamptz". All fields are combined with logical 'AND'. */
export type Timestamptz_Comparison_Exp = {
    _cast?: InputMaybe<Timestamptz_Cast_Exp>;
    _eq?: InputMaybe<Scalars['timestamptz']>;
    _gt?: InputMaybe<Scalars['timestamptz']>;
    _gte?: InputMaybe<Scalars['timestamptz']>;
    _in?: InputMaybe<Array<Scalars['timestamptz']>>;
    _is_null?: InputMaybe<Scalars['Boolean']>;
    _lt?: InputMaybe<Scalars['timestamptz']>;
    _lte?: InputMaybe<Scalars['timestamptz']>;
    _neq?: InputMaybe<Scalars['timestamptz']>;
    _nin?: InputMaybe<Array<Scalars['timestamptz']>>;
};

export type Uuid_Cast_Exp = {
    String?: InputMaybe<String_Comparison_Exp>;
};

/** Boolean expression to compare columns of type "uuid". All fields are combined with logical 'AND'. */
export type Uuid_Comparison_Exp = {
    _cast?: InputMaybe<Uuid_Cast_Exp>;
    _eq?: InputMaybe<Scalars['uuid']>;
    _gt?: InputMaybe<Scalars['uuid']>;
    _gte?: InputMaybe<Scalars['uuid']>;
    _in?: InputMaybe<Array<Scalars['uuid']>>;
    _is_null?: InputMaybe<Scalars['Boolean']>;
    _lt?: InputMaybe<Scalars['uuid']>;
    _lte?: InputMaybe<Scalars['uuid']>;
    _neq?: InputMaybe<Scalars['uuid']>;
    _nin?: InputMaybe<Array<Scalars['uuid']>>;
};

export type CreateListingMutationVariables = Exact<{
    input: App_Listings_Insert_Input;
}>;

export type CreateListingMutation = {
    __typename?: 'mutation_root';
    insert_app_listings_one?:
        | ({ __typename?: 'app_listings' } & Detailed_ListingFragment)
        | null;
};

export type DeleteListingMutationVariables = Exact<{
    id: Scalars['uuid'];
}>;

export type DeleteListingMutation = {
    __typename?: 'mutation_root';
    delete_app_listings_by_pk?: {
        __typename?: 'app_listings';
        id: string;
    } | null;
};

export type EditListingMutationVariables = Exact<{
    id: App_Listings_Pk_Columns_Input;
    input: App_Listings_Set_Input;
}>;

export type EditListingMutation = {
    __typename?: 'mutation_root';
    update_app_listings_by_pk?:
        | ({ __typename?: 'app_listings' } & Detailed_ListingFragment)
        | null;
};

export type Detailed_ListingFragment = {
    __typename?: 'app_listings';
    id: string;
    title: string;
    description?: string | null;
    price: number;
    currency: Enums_Currency_Enum;
    created_at: string;
    is_open: boolean;
    thumbnail?: { __typename?: 'app_photos'; id: string; link: string } | null;
    photos: Array<{ __typename?: 'app_photos'; id: string; link: string }>;
    creator: { __typename?: 'app_users'; id: string; name?: string | null };
};

export type Form_ListingFragment = {
    __typename?: 'app_listings';
    id: string;
    title: string;
    description?: string | null;
    price: number;
    currency: Enums_Currency_Enum;
    is_selling: boolean;
    photos: Array<{ __typename?: 'app_photos'; id: string }>;
    creator: { __typename?: 'app_users'; id: string };
};

export type List_ListingFragment = {
    __typename?: 'app_listings';
    id: string;
    title: string;
    price: number;
    currency: Enums_Currency_Enum;
    created_at: string;
    thumbnail?: { __typename?: 'app_photos'; id: string; link: string } | null;
    creator: { __typename?: 'app_users'; id: string; name?: string | null };
};

export type GetFormListingQueryVariables = Exact<{
    id: Scalars['uuid'];
}>;

export type GetFormListingQuery = {
    __typename?: 'query_root';
    app_listings_by_pk?:
        | ({ __typename?: 'app_listings' } & Form_ListingFragment)
        | null;
};

export type GetListingQueryVariables = Exact<{
    id: Scalars['uuid'];
}>;

export type GetListingQuery = {
    __typename?: 'query_root';
    app_listings_by_pk?:
        | ({ __typename?: 'app_listings' } & Detailed_ListingFragment)
        | null;
};

export type GetListingsQueryVariables = Exact<{
    filter?: InputMaybe<App_Listings_Bool_Exp>;
    limit: Scalars['Int'];
    offset: Scalars['Int'];
    order_by: App_Listings_Order_By;
}>;

export type GetListingsQuery = {
    __typename?: 'query_root';
    app_listings: Array<{ __typename?: 'app_listings' } & List_ListingFragment>;
    app_listings_aggregate: {
        __typename?: 'app_listings_aggregate';
        aggregate?: {
            __typename?: 'app_listings_aggregate_fields';
            count: number;
        } | null;
    };
};

export type UpsertFormListingMutationVariables = Exact<{
    input: App_Listings_Insert_Input;
}>;

export type UpsertFormListingMutation = {
    __typename?: 'mutation_root';
    insert_app_listings?: {
        __typename?: 'app_listings_mutation_response';
        returning: Array<
            { __typename?: 'app_listings' } & Form_ListingFragment
        >;
    } | null;
};

export type AddPhotoMutationVariables = Exact<{
    photo: App_Photos_Insert_Input;
}>;

export type AddPhotoMutation = {
    __typename?: 'mutation_root';
    insert_app_photos_one?: { __typename?: 'app_photos'; id: string } | null;
};

export type Photo_LinkFragment = { __typename?: 'app_photos'; link: string };

export type GetPhotosQueryVariables = Exact<{
    listing_id: Scalars['uuid'];
}>;

export type GetPhotosQuery = {
    __typename?: 'query_root';
    app_photos: Array<{ __typename?: 'app_photos' } & Photo_LinkFragment>;
};

export type UploadPhotoMutationVariables = Exact<{
    b64File: Scalars['String'];
    filename: Scalars['String'];
    mimeType: Scalars['String'];
}>;

export type UploadPhotoMutation = {
    __typename?: 'mutation_root';
    uploadPublicFile?: {
        __typename?: 'FileResource';
        id: string;
        name: string;
        mimeType: string;
        resourceUrl: string;
    } | null;
};

export type CreateUserMutationVariables = Exact<{
    input: App_Users_Insert_Input;
}>;

export type CreateUserMutation = {
    __typename?: 'mutation_root';
    insert_app_users_one?:
        | ({ __typename?: 'app_users' } & User_ProfileFragment)
        | null;
};

export type User_ProfileFragment = {
    __typename?: 'app_users';
    id: string;
    name?: string | null;
    email: string;
    listings: Array<{ __typename?: 'app_listings' } & List_ListingFragment>;
};

export type GetUserProfileQueryVariables = Exact<{
    id: Scalars['uuid'];
}>;

export type GetUserProfileQuery = {
    __typename?: 'query_root';
    app_users_by_pk?:
        | ({ __typename?: 'app_users' } & User_ProfileFragment)
        | null;
};

export type UpdateUserProfileMutationVariables = Exact<{
    id: App_Users_Pk_Columns_Input;
    input: App_Users_Set_Input;
}>;

export type UpdateUserProfileMutation = {
    __typename?: 'mutation_root';
    update_app_users_by_pk?:
        | ({ __typename?: 'app_users' } & User_ProfileFragment)
        | null;
};
